using System;
using System.IO;
using System.Xml;
using System.Text;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.Security.Principal;
//using System.Windows.Forms;

namespace siebelClientShortcuts
{
	internal static class commonMethods
	{
		public static string configFile;

        public static XmlDocument config;

		public static string logErrorFile;

        public static string serverPath;

        public static string passwd;

        public static string alias;

        public static string fileExport;

        public static string fileLog;

        public static string AppVersion = "3.5";

        public static string login = WindowsIdentity.GetCurrent().Name.Substring(WindowsIdentity.GetCurrent().Name.LastIndexOf(@"\") + 1);

        //public NotifyIcon notifyIcon1;

        public static string serverName;
        public static string iNombreServidor ;
        public static string iDescripcion ;
        public static string iServicioWindows ;
        public static string iDestinoSRF ;
        public static string iOrigenSRF ;
        public static string iRutaLog ;
        public static string iRutaBin ;
        public static string iArchivoLOGMonitoreo  ;
        public static string iGateWay ;
        public static string iEnterprise ;
        public static string iConnectionString ;
        public static string iCompila ;
        public static string iServidor ;
        public static string iDestinoDAT ;
        public static string iRutaInst ;
        public static string iRutaTools ;
        public static string iRutaBrowsersScripts1 ;
        public static string iRutaBrowsersScripts2 ;
        public static string iPerfil ;
        public static string iODBC ;
        public static string iPasswordSADMIN ;
        public static string iPasswordSIEBEL ;
        public static string iRutaToolsBin ;
        public static string iRutaSchema ;
        public static string iPasswordURL;

		static commonMethods()
		{
            if (!Directory.Exists(string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "\\siebelLauncher")))
            {
                System.IO.Directory.CreateDirectory(string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "\\siebelLauncher"));
            }
			commonMethods.configFile = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "\\siebelLauncher\\Config.xml");
			string[] folderPath = new string[] { Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "\\siebelLauncher\\log_", null, null, null, null, null, null };
			DateTime date = DateTime.Now.Date;
			int day = date.Day;
			folderPath[2] = day.ToString();
			date = DateTime.Now.Date;
			day = date.Month;
			folderPath[3] = day.ToString();
			date = DateTime.Now.Date;
			day = date.Year;
			folderPath[4] = day.ToString();
			folderPath[5] = "_"; 
			day = DateTime.Now.Hour;
			folderPath[6] = day.ToString();
			folderPath[7] = "hs.log";
			commonMethods.logErrorFile = string.Concat(folderPath);
            commonMethods.config = new XmlDocument();
		}

		public static void logMessage(string message, Exception e = null)
		{
            DateTime now = DateTime.Now;
			FileStream fileStream = null;
			FileInfo fileInfo = new FileInfo(commonMethods.logErrorFile);
			fileStream = (fileInfo.Exists ? new FileStream(commonMethods.logErrorFile, FileMode.Append) : fileInfo.Create());
			StreamWriter streamWriter = new StreamWriter(fileStream);
			streamWriter.WriteLine(string.Concat(now.ToString(), ": " ,message));
			if (e != null)
			{
				streamWriter.WriteLine(e.ToString());
			}
			streamWriter.Close();

		}

        public static void createXMLFile(string prevInstpath, string prevSRFLocalPath, string prevSQLTracePath, string prevDesaName, string prevInteName)
        {
            Directory.CreateDirectory(string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "\\siebelLauncher\\"));
            XmlDocument xmlDocument = new XmlDocument();
            XmlDeclaration xmlDeclaration = xmlDocument.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmlDocument.InsertBefore(xmlDeclaration, xmlDocument.DocumentElement);
            XmlElement parameters = xmlDocument.CreateElement("Parameters");
            xmlDocument.AppendChild(parameters);
            XmlElement appversion = xmlDocument.CreateElement("AppVersion");
            parameters.AppendChild(appversion);
            appversion.InnerXml = AppVersion;
            parameters.AppendChild(xmlDocument.CreateComment("Use Dark Theme"));
            XmlElement theme = xmlDocument.CreateElement("Theme");
            parameters.AppendChild(theme);
            theme.InnerXml = "false";
            parameters.AppendChild(xmlDocument.CreateComment("Usuario de Siebel"));
            XmlElement LoginName = xmlDocument.CreateElement("LoginName");
            parameters.AppendChild(LoginName);
            LoginName.InnerXml = login;
            parameters.AppendChild(xmlDocument.CreateComment("DESA/INTE"));
            XmlElement Envirorment = xmlDocument.CreateElement("Envirorment");
            parameters.AppendChild(Envirorment);
            Envirorment.InnerXml = "DESA";
            parameters.AppendChild(xmlDocument.CreateComment("IE/CHROME"));
            XmlElement Browser = xmlDocument.CreateElement("Browser");
            parameters.AppendChild(Browser);
            Browser.InnerXml = "IE";
            parameters.AppendChild(xmlDocument.CreateComment("FINS/PORTAL"));
            XmlElement Application = xmlDocument.CreateElement("Application");
            parameters.AppendChild(Application);
            Application.InnerXml = "FINS";
            parameters.AppendChild(xmlDocument.CreateComment("TRUE/FALSE"));
            XmlElement DebugMode = xmlDocument.CreateElement("DebugMode");
            parameters.AppendChild(DebugMode);
            DebugMode.InnerXml = "false";
            parameters.AppendChild(xmlDocument.CreateComment("TRUE/FALSE"));
            XmlElement SQLTrace = xmlDocument.CreateElement("SQLTrace");
            parameters.AppendChild(SQLTrace);
            SQLTrace.InnerXml = "false";
            parameters.AppendChild(xmlDocument.CreateComment("PATH where to generate SQL Trace"));
            XmlElement SQLTracePath = xmlDocument.CreateElement("SQLTracePath");
            parameters.AppendChild(SQLTracePath);
            SQLTracePath.InnerXml = (prevSQLTracePath != null ? prevSQLTracePath : "c:\\");
            parameters.AppendChild(xmlDocument.CreateComment("Path up to 'Client' folder"));
            XmlElement SiebelInstallationPath = xmlDocument.CreateElement("SiebelInstallationPath");
            parameters.AppendChild(SiebelInstallationPath);
            SiebelInstallationPath.InnerXml = (prevInstpath != null ? prevInstpath : "C:\\Siebel\\Client");
            parameters.AppendChild(xmlDocument.CreateComment("CFG envirorment's name"));
            XmlElement CFGConfig = xmlDocument.CreateElement("CFGConfig");
            parameters.AppendChild(CFGConfig);
            parameters.AppendChild(xmlDocument.CreateComment("Path where Siebel Client look for the SRF file"));
            XmlElement SRFLocalPath = xmlDocument.CreateElement("SRFLocalPath");
            CFGConfig.AppendChild(SRFLocalPath);
            SRFLocalPath.InnerXml = (prevSRFLocalPath != null ? prevSRFLocalPath : string.Concat("C:\\Users\\", login, "\\Desktop"));
            XmlElement DesaName = xmlDocument.CreateElement("DesaName");
            CFGConfig.AppendChild(DesaName);
            DesaName.InnerXml = (prevDesaName != null ? prevDesaName : "Desa02");
            XmlElement InteName = xmlDocument.CreateElement("InteName");
            CFGConfig.AppendChild(InteName);
            InteName.InnerXml = (prevInteName != null ? prevInteName : "Inte02");
            XmlElement HomoName = xmlDocument.CreateElement("HomoName");
            CFGConfig.AppendChild(HomoName);
            HomoName.InnerXml = "Homo01";
            parameters.AppendChild(xmlDocument.CreateComment("Server's names"));
            XmlElement Servers = xmlDocument.CreateElement("Servers");
            parameters.AppendChild(Servers);
            XmlElement ServerDesa = xmlDocument.CreateElement("Desarrollo");
            Servers.AppendChild(ServerDesa);
            ServerDesa.InnerXml = "dappsbl02";
            XmlElement ServerInte = xmlDocument.CreateElement("Integracion");
            Servers.AppendChild(ServerInte);
            ServerInte.InnerXml = "iappsbl02";
            parameters.AppendChild(xmlDocument.CreateComment("Enterprise for Siebel Console server"));
            XmlElement Enterprise = xmlDocument.CreateElement("Enterprise");
            parameters.AppendChild(Enterprise);
            XmlElement DesarrolloEnt = xmlDocument.CreateElement("Desarrollo");
            Enterprise.AppendChild(DesarrolloEnt);
            DesarrolloEnt.InnerXml = "ENT_DESA02";
            XmlElement IntegracionEnt = xmlDocument.CreateElement("Integracion");
            Enterprise.AppendChild(IntegracionEnt);
            IntegracionEnt.InnerXml = "ENT_INT";
            XmlElement HolomogacionEnt = xmlDocument.CreateElement("Homologacion");
            Enterprise.AppendChild(HolomogacionEnt);
            HolomogacionEnt.InnerXml = "HOMO_ENT";
            parameters.AppendChild(xmlDocument.CreateComment("TNS Name as shown in tnsnames.ora in folder %TNS_ADMIN%"));
            XmlElement TNSNames = xmlDocument.CreateElement("TNSNames");
            parameters.AppendChild(TNSNames);
            XmlElement TNSDesa = xmlDocument.CreateElement("Desarrollo");
            TNSNames.AppendChild(TNSDesa);
            TNSDesa.InnerXml = "DESA02_SIEBELDB";
            XmlElement TNSInte = xmlDocument.CreateElement("Integracion");
            TNSNames.AppendChild(TNSInte);
            TNSInte.InnerXml = "INT_SIEBELDB";
            XmlElement TNSHomo = xmlDocument.CreateElement("Homologacion");
            TNSNames.AppendChild(TNSHomo);
            TNSHomo.InnerXml = "HOMO01_SIEBELDB";
            XmlElement TNSProd = xmlDocument.CreateElement("Produccion");
            TNSNames.AppendChild(TNSProd);
            TNSProd.InnerXml = "SIEBELP";
            xmlDocument.Save(commonMethods.configFile);
            commonMethods.logMessage(String.Concat("creo el archivo .config: ", DateTime.Now.ToString()), null);
        }      

        public static void ServerConfig(string server)
        {
            claseEncriptar descr = new claseEncriptar();
            System.Data.DataTable dataTable = new System.Data.DataTable();
            string connetionString = null;
            object obj1 = 0;
            SqlConnection sqlConnection1;
            connetionString = "Data Source = TCP: Siebel8DesaApp, 4000; Initial Catalog = Tools_PasajesGreco; user id= ToolsPasaje; password= oytp524o";
            sqlConnection1 = new SqlConnection(connetionString);
            try
            {
                sqlConnection1.Open();
                commonMethods.logMessage(string.Concat("Conexion establecida con BD"), null);
                (new SqlDataAdapter("select * from ConfiguracionCompilacionDESAORA", sqlConnection1)).Fill(dataTable);
                sqlConnection1.Close();
                
                foreach( System.Data.DataRow row in dataTable.Rows)
                 {
                    iNombreServidor = row["Servidor"].ToString();
                    iDescripcion = row["DescripcionSrv"].ToString();
                    iServicioWindows = row["ServicioWindows"].ToString();
                    iDestinoSRF = row["RutaSRFServidor"].ToString();
                    iOrigenSRF = row["RutaSRFCompilacion"].ToString();
                    iRutaLog = row["RutaLogs"].ToString();
                    iRutaBin = row["RutaBINSiebelServer"].ToString();
                    iArchivoLOGMonitoreo = row["MonitoreoArchivoLogENT"].ToString();
                    iGateWay = row["Gateway"].ToString();
                    iEnterprise = row["EnterpriseSiebel"].ToString();
                    iConnectionString = row["ConectionString"].ToString();
                    iCompila = row["CompilaCadena"].ToString();
                    iDestinoDAT = row["RutaDAT"].ToString();
                    iRutaInst = row["RutaInstSiebel"].ToString();
                    iRutaTools = row["RutaLocalTools"].ToString();
                    iRutaBrowsersScripts1 = row["RutaBrowsersScripts1"].ToString();
                    iRutaBrowsersScripts2 = row["RutaBrowsersScripts2"].ToString();
                    iPerfil = row["Perfil"].ToString();
                    iODBC = row["ODBC"].ToString();
                    iPasswordSADMIN = descr.Desencriptar(row["PasswordSADMIN"].ToString());
                    iPasswordSIEBEL = descr.Desencriptar(row["PasswordSIEBEL"].ToString());
                    iRutaToolsBin = row["RutaTools"].ToString();
                    iRutaSchema = row["RutaSchema"].ToString();
                    iPasswordURL = row["PasswordURL"].ToString();

                    //iConnectionString = string.Concat(Strings.Trim(dataTable.Rows[Conversions.ToInteger(obj1)]["ConectionString"].ToString()), Declaracion_de_Variables.mtzServer(Conversions.ToInteger(obj1), Conversions.ToInteger(Declaracion_de_Variables.iPasswordSADMIN)));
                    //iCompila = string.Concat(new string[] { " /c ", Declaracion_de_Variables.mtzServer(Conversions.ToInteger(obj1), Conversions.ToInteger(Declaracion_de_Variables.iRutaToolsBin)), @"\enu\tools.cfg /u SADMIN /p ", Declaracion_de_Variables.mtzServer(Conversions.ToInteger(obj1), Conversions.ToInteger(Declaracion_de_Variables.iPasswordSADMIN)), " /tl ESN /d ", Strings.Trim(dataTable.Rows[Conversions.ToInteger(obj1)]["CompilaCadena"].ToString()), " /bc 'Siebel Repository' ", Declaracion_de_Variables.mtzServer(Conversions.ToInteger(obj1), Conversions.ToInteger(Declaracion_de_Variables.iOrigenSRF)) });
                 }
                
                
            }
            catch (Exception exception)
            {
                commonMethods.logMessage(string.Concat("Error al obtener parametros del servidor: "), exception.InnerException);
            }

            if (server == "Desa02")
            { 
                //do something
                serverName = "DAPPSBL02";
                serverPath = "\\\\DAPPSBL02\\d$\\Siebel\\Tools\\BIN\\repimexp.exe";
                passwd = "SADMINgreco2016";
                alias = config.SelectSingleNode("Parameters/TNSNames/Desarrollo").InnerXml;
                fileExport = "\\\\DAPPSBL02\\DAT\\customer_export.dat";
                fileLog = "\\\\DAPPSBL02\\DAT\\DAPPSBL02.log";
            }
            if (server == "Desa03")
            {
                //do something
                serverName = "DAPPSBL03";
                serverPath = "T:\\Siebel\\ses\\siebsrvr\\BIN\\repimexp.exe";
                passwd = "SADMINgreco2016";
                alias = "DESA03_SIEBELDB";
                fileExport = "T:\\Siebel\\Compilacion\\DAT\\customer_export.dat";
                fileLog = "T:\\Siebel\\Compilacion\\DAT\\DAPPSBL03.log";
            }
            if (server == "Inte02")
            {
                //do something
                serverName = "IAPPSBL02";
                serverPath = "\\\\IAPPSBL02\\d$\\Siebel\\Tools\\BIN\\repimexp.exe";
                passwd = "SADMINgreco2016";
                alias = config.SelectSingleNode("Parameters/TNSNames/Integracion").InnerXml;
                fileExport = "\\\\IAPPSBL02\\DAT\\customer_export.dat";
                fileLog = "\\\\IAPPSBL02\\DAT\\IAPPSBL02.log";
                //test commit
            }
        }
	}
}