using System;
using System.Drawing;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace siebelClientShortcuts
{
	internal class DialogCenteringService : IDisposable
	{
		private const int WH_CALLWNDPROCRET = 12;

		private readonly IWin32Window owner;

		private readonly DialogCenteringService.HookProc hookProc;

		private readonly IntPtr hHook = IntPtr.Zero;

		public DialogCenteringService(IWin32Window owner)
		{
			if (owner == null)
			{
				throw new ArgumentNullException("owner");
			}
			this.owner = owner;
			this.hookProc = new DialogCenteringService.HookProc(this.DialogHookProc);
			this.hHook = DialogCenteringService.SetWindowsHookEx(12, this.hookProc, IntPtr.Zero, DialogCenteringService.GetCurrentThreadId());
		}

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern IntPtr CallNextHookEx(IntPtr idHook, int nCode, IntPtr wParam, IntPtr lParam);

		private void CenterWindow(IntPtr hChildWnd)
		{
			Rectangle rectangle = new Rectangle(0, 0, 0, 0);
			if (DialogCenteringService.GetWindowRect(hChildWnd, ref rectangle))
			{
				int width = rectangle.Width - rectangle.X;
				int height = rectangle.Height - rectangle.Y;
				Rectangle rectangle1 = new Rectangle(0, 0, 0, 0);
				if (DialogCenteringService.GetWindowRect(this.owner.Handle, ref rectangle1))
				{
					Point point = new Point(0, 0)
					{
						X = rectangle1.X + (rectangle1.Width - rectangle1.X) / 2,
						Y = rectangle1.Y + (rectangle1.Height - rectangle1.Y) / 2
					};
					Point point1 = new Point(0, 0)
					{
						X = point.X - width / 2,
						Y = point.Y - height / 2
					};
					Task.Factory.StartNew<bool>(() => DialogCenteringService.SetWindowPos(hChildWnd, (IntPtr)0, point1.X, point1.Y, width, height, SetWindowPosFlags.SWP_ASYNCWINDOWPOS | SetWindowPosFlags.SWP_NOACTIVATE | SetWindowPosFlags.SWP_NOOWNERZORDER | SetWindowPosFlags.SWP_NOREPOSITION | SetWindowPosFlags.SWP_NOSIZE | SetWindowPosFlags.SWP_NOZORDER));
				}
			}
		}

		private IntPtr DialogHookProc(int nCode, IntPtr wParam, IntPtr lParam)
		{
			IntPtr intPtr;
			if (nCode >= 0)
			{
				DialogCenteringService.CWPRETSTRUCT structure = (DialogCenteringService.CWPRETSTRUCT)Marshal.PtrToStructure(lParam, typeof(DialogCenteringService.CWPRETSTRUCT));
				IntPtr intPtr1 = this.hHook;
				if (structure.message == 5)
				{
					try
					{
						this.CenterWindow(structure.hwnd);
					}
					finally
					{
						DialogCenteringService.UnhookWindowsHookEx(this.hHook);
					}
				}
				intPtr = DialogCenteringService.CallNextHookEx(intPtr1, nCode, wParam, lParam);
			}
			else
			{
				intPtr = DialogCenteringService.CallNextHookEx(this.hHook, nCode, wParam, lParam);
			}
			return intPtr;
		}

		public void Dispose()
		{
			DialogCenteringService.UnhookWindowsHookEx(this.hHook);
		}

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int EndDialog(IntPtr hDlg, IntPtr nResult);

		[DllImport("kernel32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern int GetCurrentThreadId();

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern bool GetWindowRect(IntPtr hWnd, ref Rectangle lpRect);

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int GetWindowText(IntPtr hWnd, StringBuilder text, int maxLength);

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int GetWindowTextLength(IntPtr hWnd);

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern int MoveWindow(IntPtr hWnd, int X, int Y, int nWidth, int nHeight, bool bRepaint);

		[DllImport("User32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern IntPtr SendMessage(IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

		[DllImport("User32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern UIntPtr SetTimer(IntPtr hWnd, UIntPtr nIDEvent, uint uElapse, DialogCenteringService.TimerProc lpTimerFunc);

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		private static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, SetWindowPosFlags uFlags);

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern IntPtr SetWindowsHookEx(int idHook, DialogCenteringService.HookProc lpfn, IntPtr hInstance, int threadId);

		[DllImport("user32.dll", CharSet=CharSet.None, ExactSpelling=false)]
		public static extern int UnhookWindowsHookEx(IntPtr idHook);

		private enum CbtHookAction
		{
			HCBT_MOVESIZE,
			HCBT_MINMAX,
			HCBT_QS,
			HCBT_CREATEWND,
			HCBT_DESTROYWND,
			HCBT_ACTIVATE,
			HCBT_CLICKSKIPPED,
			HCBT_KEYSKIPPED,
			HCBT_SYSCOMMAND,
			HCBT_SETFOCUS
		}

		public struct CWPRETSTRUCT
		{
			public IntPtr lResult;

			public IntPtr lParam;

			public IntPtr wParam;

			public uint message;

			public IntPtr hwnd;
		}

		public delegate IntPtr HookProc(int nCode, IntPtr wParam, IntPtr lParam);

		public delegate void TimerProc(IntPtr hWnd, uint uMsg, UIntPtr nIDEvent, uint dwTime);
	}
}