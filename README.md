# SiebelLauncher

arma y ejecuta el comando para abrir Siebel de forma local, conectandose a diferentes sevidores y distintos usuarios de forma facil.

### Botones
- **Lanzar Siebel Client**: arma el comando segun los parametros seleccionados de Ambiente, Navegador, Aplicacion y usuario.
- **Bajar archivos custom**: Bajas del servidor a la local los archivos .js y template para la correcta ejecucion de SiebelMobileClient.
- **Generar BrowserScript**: Genera los browser dentro de la carpeta seleccionada dentro de la configuracion.
- **Matar Siebel.exe**: mata todos los procesos en ejecucion de Siebel Mobile Client.
- **Matar iexplore.exe**: mata todos los procesos en ejecucion de Internet Explorer.

### Menu
- Copiado de SRF a carpeta local
- Copiado de SRF local a la carpeta SRF del servidor 
- Abrir archivo CFG (fins, loyalty y tools) que utilizara el programa
- Abrir y borrar los Logs local
- Ejecutar la Consola Siebel y SQLPlus 
- Abrir el archivo TNS Names (depende de la variable TNS_ADMIN no siempre funciona)

### Configuracion:
1. Seleccionar donde se encuentra instalado Siebel
1. Seleccionar donde se desea guardar en spool de SQL
1. Seleccionar donde se desea copiar el SRF traido del servidor
1. Ingresar los nombres de los DataSources tal cual como se encuentran en el CFG
1. Ingresar los alias que se encuentran dentro del TNS Names para las distintas bases de datos.

---

## Proximas funcionalidades (not gonna happn)
- Realizar full compile en el servidor
- Mejoras al servicio de Exportar DAT
- Realizar Sincronizacion de equema en el servidor
- MatarIE que mate tambien siebel.exe

---

## Creditos
- Matias Brigante
- Lucas Pehuen Solari
