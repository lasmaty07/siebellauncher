﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace siebelClientShortcuts
{
    public class claseEncriptar
    {
        private TripleDESCryptoServiceProvider m_des;

        private UTF8Encoding m_utf8;

        private byte[] m_key;

        private byte[] m_iv;

        private readonly byte[] key;

        private readonly byte[] iv;

        public claseEncriptar()
            : base()
        {
            this.m_des = new TripleDESCryptoServiceProvider();
            this.m_utf8 = new UTF8Encoding();
            this.key = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24 };
            this.iv = new byte[] { 45, 18, 44, 35, 56, 32, 41, 14 };
            this.m_key = this.key;
            this.m_iv = this.iv;
        }

        public byte[] Desencriptar(byte[] input)
        {
            return this.Transformar(input, this.m_des.CreateDecryptor(this.m_key, this.m_iv));
        }

        public string Desencriptar(string text)
        {
            byte[] numArray = Convert.FromBase64String(text);
            byte[] numArray1 = this.Transformar(numArray, this.m_des.CreateDecryptor(this.m_key, this.m_iv));
            return this.m_utf8.GetString(numArray1);
        }

        public byte[] Encriptar(byte[] input)
        {
            return this.Transformar(input, this.m_des.CreateEncryptor(this.m_key, this.m_iv));
        }

        public string Encriptar(string text)
        {
            byte[] bytes = this.m_utf8.GetBytes(text);
            return Convert.ToBase64String(this.Transformar(bytes, this.m_des.CreateEncryptor(this.m_key, this.m_iv)));
        }

        private byte[] Transformar(byte[] input, ICryptoTransform CryptoTransformar)
        {
            System.IO.MemoryStream memoryStream = new System.IO.MemoryStream();
            System.Security.Cryptography.CryptoStream cryptoStream = new System.Security.Cryptography.CryptoStream(memoryStream, CryptoTransformar, CryptoStreamMode.Write);
            cryptoStream.Write(input, 0, System.Convert.ToInt32(input.Length));
            cryptoStream.FlushFinalBlock();
            memoryStream.Position = System.Convert.ToInt64(0);
            byte[] numArray = new byte[System.Convert.ToInt32((memoryStream.Length - System.Convert.ToInt64(1))) + 1 - 1 + 1];
            memoryStream.Read(numArray, 0, System.Convert.ToInt32(numArray.Length));
            memoryStream.Close();
            cryptoStream.Close();
            return numArray;
        }
    }
}
