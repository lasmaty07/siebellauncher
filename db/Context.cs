﻿using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using siebelClientShortcuts.db;

namespace siebelClientShortcuts.Model
{
    public class MyContext : DbContext
    {
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        { 
            var sqliteConnectionInitializer = new SiebelDBInitializer(modelBuilder);
            Database.SetInitializer(sqliteConnectionInitializer);            
        }
        public DbSet<Servers> Servers { get; set; }
        public DbSet<AppInfo> AppInfo { get; set; }
        public DbSet<ConfigSiebel> ConfigSiebel { get; set; }
        public DbSet<TNSNames> TNSNames { get; set; }
    }
}