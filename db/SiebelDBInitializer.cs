﻿using siebelClientShortcuts.Model;
using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Text;

namespace siebelClientShortcuts.db
{
    public class SiebelDBInitializer : SqliteDropCreateDatabaseWhenModelChanges<MyContext>
    {
        public SiebelDBInitializer(DbModelBuilder modelBuilder)
            : base(modelBuilder) { }

        protected override void Seed(MyContext context)
        {
            IList<Servers> servers = new List<Servers>();

            servers.Add(new Servers()
            {
	            ServidorId = "DAPPSBL02",
	            ServicioWindows = "siebsrvr_ENT_DESA02_DAPPSBL02",
	            RutaSRFServidor = "\\\\DAPPSBL02\\ESN$\\siebel_sia.srf",
	            RutaSRFCompilacion = "\\\\DAPPSBL02\\SRF\\siebel_sia.srf",
	            RutaLogs = "\\\\DAPPSBL02\\siebsrvr\\log",
	            RutaDAT = "\\\\DAPPSBL02\\DAT\\customer_export.dat",
	            RutaBINSiebelServer = "D:\\Siebel\\\\ses\\siebsrvr\\BIN",
	            RutaInstSiebel = "D:\\Siebel\\ses",
	            RutaLocalTools = "D:\\Siebel\\Tools\\bin\\siebdev.exe",
	            MonitoreoArchivoLogENT = "ENT_DESA02.DAPPSBL02.LOG",
	            EnterpriseSiebel = "ENT_DESA02",
	            CompilaCadena = "ServerDataSrcDesa215",
	            ConectionString = "Data Source=DESA02_SIEBELDB; User Id=SADMIN;Password= ",
	            Perfil = "DESARROLLO",	            
                Gateway = "DAPPSBL02",
	            DescripcionSrv = "Servidor de Desarrollo",	            
                RutaBrowsersScripts1 = "\\\\dappsbl02\\d$\\Siebel\\eappweb\\PUBLIC\\esn",
	            RutaBrowsersScripts2 = "\\\\dappsbl02\\d$\\Siebel\\ses\\siebsrvr\\WEBMASTER",
	            ODBC = "DESA02_SIEBELDB",
	            PasswordSADMIN = "kvfS6YR45cOerP3gj11fgQ==",
	            PasswordSIEBEL = "L3C1gTaFPF1TzrCzQZUrkSHf0TcbOKzg",
	            RutaTools = "D:\\Siebel\\Tools\\bin",
	            RutaSchema = "\\\\dappsbl02\\d$\\Siebel\\ses\\dbsrvr\\ORACLE"
            });
            servers.Add(new Servers()
            {
                ServidorId = "DAPPSBL03",
                ServicioWindows = "siebsrvr_ENT_DESA03",
                RutaSRFServidor = "\\\\DAPPSBL03\\ESN$\\siebel_sia.srf",
                RutaSRFCompilacion = "\\\\DAPPSBL03\\SRF\\siebel_sia.srf",
                RutaLogs = "\\\\DAPPSBL03\\siebsrvr\\log",
                RutaDAT = "\\\\DAPPSBL02\\DAT\\customer_export.dat",
                RutaBINSiebelServer = "T:\\Siebel\\\\ses\\siebsrvr\\BIN",
                RutaInstSiebel = "T:\\Siebel\\ses",
                RutaLocalTools = "D:\\Siebel\\Tools\\bin\\siebdev.exe",
                MonitoreoArchivoLogENT = "ENT_DESA03.DAPPSBL02.LOG",
                EnterpriseSiebel = "ENT_DESA03",
                CompilaCadena = "ServerDataSrcDesaBF15",
                ConectionString = "Data Source=DESA03_SIEBELDB; User Id=SADMIN; Password= ",
                Perfil = "DESARROLLO",
                Gateway = "DAPPSBL03",
                DescripcionSrv = "Servidor de Desarrollo",
                RutaBrowsersScripts1 = "\\\\DAPPSBL03\\T$\\Siebel\\eappweb\\PUBLIC\\esn",
                RutaBrowsersScripts2 = "\\\\DAPPSBL03\\T$\\Siebel\\ses\\siebsrvr\\WEBMASTER",
                ODBC = "DESA03_SIEBELDB",
                PasswordSADMIN = "kvfS6YR45cOerP3gj11fgQ==",
                PasswordSIEBEL = "L3C1gTaFPF1TzrCzQZUrkSHf0TcbOKzg",
                RutaTools = "D:\\Siebel\\Tools\\bin",
                RutaSchema = "\\\\dappsbl02\\d$\\Siebel\\ses\\dbsrvr\\ORACLE"
            });
            servers.Add(new Servers()
            {
	            ServidorId = "IAPPSBL02",
	            ServicioWindows = "siebsrvr_ENT_INT_IAPPSBL02",
	            RutaSRFServidor = "\\\\IAPPSBL02\\ESN$\\siebel_sia.srf",
	            RutaSRFCompilacion = "\\\\IAPPSBL02\\\\SRF\\siebel_sia.srf",
	            RutaLogs = "\\\\IAPPSBL02\\siebsrvr\\log",
	            RutaDAT = "\\\\IAPPSBL02\\DAT\\customer_export.dat",
	            RutaBINSiebelServer = "D:\\Siebel\\\\ses\\siebsrvr\\BIN",
	            RutaInstSiebel = "D:\\Siebel\\ses",
                RutaLocalTools = "D:\\Siebel\\Tools\\bin\\siebdev.exe",
	            MonitoreoArchivoLogENT = "ENT_DESA01.DAPPSBL02.LOG",
	            EnterpriseSiebel = "ENT_DESA01",
	            CompilaCadena = "ServerDataSrcDesa15",
	            ConectionString = "Data Source=INT_SIEBELDB; User Id=SADMIN; Password= ",
	            Perfil = "INTEGRACION",
	            Gateway = "IAPPSBL02",
	            DescripcionSrv = "Servidor de Integracion",
	            RutaBrowsersScripts1 = "\\\\IAPPSBL02\\d$\\Siebel\\eappweb\\PUBLIC\\esn",
	            RutaBrowsersScripts2 = "\\\\IAPPSBL02\\d$\\Siebel\\ses\\siebsrvr\\WEBMASTER",
	            ODBC = "INT_SIEBELDB",
	            PasswordSADMIN = "3I5NgBVncBV3gSEPkB6INg==",
	            PasswordSIEBEL = "nR20O1ze1UhBPke0PGkQKw==",
	            RutaTools = "D:\\Siebel\\Tools\\bin",
	            RutaSchema = "\\\\IAPPSBL02\\d$\\Siebel\\ses\\dbsrvr\\ORACLE",
	            Activo = "Y",
	            Debug = "N"
            });

            AppInfo appinfo = new AppInfo() { AppVersion = "4.0.0" };

            IList<TNSNames> tnsNames = new List<TNSNames>();

            tnsNames.Add(new TNSNames() { Ambiente = "desarrollo", Alias = "DESARROLLOSiebel" });
            tnsNames.Add(new TNSNames() { Ambiente = "integracion", Alias = "INTEGRACIONSiebel" });
            tnsNames.Add(new TNSNames() { Ambiente = "homologacion", Alias = "HOMOLOGACIONSiebel" });
            tnsNames.Add(new TNSNames() { Ambiente = "produccion", Alias = "PRODUCCIONSiebel" });

            ConfigSiebel configSiebel = new ConfigSiebel()
            {
                LoginName = WindowsIdentity.GetCurrent().Name.Substring(WindowsIdentity.GetCurrent().Name.LastIndexOf(@"\") + 1),
                Envirorment = "DESA",
                Browser = "IE",
                Application = "FINS",
                DebugMode = "false",
                SQLTrace = "false",
                SQLTracePath = "C:\\Siebel\\Logs",
                SiebelInstallationPath = "C:\\Siebel\\Client",
                SRFLocalPath = "C:\\Siebel\\Client\\OBJECTS\\esn",
                DesaName = "Desa",
                InteName = "Inte",
                HomoName = "Homo01"
            };


            context.Servers.AddRange(servers);
            context.AppInfo.Add(appinfo);
            context.ConfigSiebel.Add(configSiebel);
            context.TNSNames.AddRange(tnsNames);

            base.Seed(context);
        }
    }
}
