﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.IO;


namespace siebelClientShortcuts
{
    
    class Scheduler
    {
        private static Timer aTimer;



        public static void time()
        {
            // Create a timer with ms interval.
            //aTimer = new System.Timers.Timer(15000);
            aTimer = new System.Timers.Timer(600000);
            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;
            aTimer.Enabled = true;
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
           // string envVar = Path.GetPathRoot(Environment.GetEnvironmentVariable("SIEBEL_LOG_DIR"));            

            //commonMethods.logMessage(string.Concat("Abrir carpeta Logs: ", DirSize(Path.GetDirectoryName(envVar))), null);
        }

        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }
    }
}
