using System;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace siebelClientShortcuts
{
	internal static class Program
	{
		[STAThread]
		private static void Main()
		{
			bool flag = true;
			Mutex mutex = new Mutex(true, "SiebelShortcut", out flag);
            
            /*
            using (var db = new Model.MyContext())
            {
                var server = new Model.Servers() { ServidorId = "Inte" };
                db.Servers.Add(server);
                db.SaveChanges();
            }*/


			try
			{
				if (!flag)
				{
					Process currentProcess = Process.GetCurrentProcess();
					Process[] processesByName = Process.GetProcessesByName(currentProcess.ProcessName);
					int num = 0;
					while (num < (int)processesByName.Length)
					{
						Process process = processesByName[num];
						if (process.Id == currentProcess.Id)
						{
							num++;
						}
						else
						{
							process.Kill();
							Application.EnableVisualStyles();
							Application.SetCompatibleTextRenderingDefault(false);
							Application.Run(new Form1());
							break;
						}
					}
				}
				else
				{
					Application.EnableVisualStyles();
					Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new Form1()); 
                    //Scheduler.time();
				}
			}
			finally
			{
				if (mutex != null)
				{
					((IDisposable)mutex).Dispose();
				}
			}
		}
	}
}