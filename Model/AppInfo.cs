﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace siebelClientShortcuts.Model
{
    [Table("AppInfo")]
    public class AppInfo
    {
        [Key]
        public int Id { get; set; }
        public string AppVersion { get; set; }
        public string App { get; set; }
    }
}