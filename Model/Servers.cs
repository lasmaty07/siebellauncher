﻿using SQLite.CodeFirst;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace siebelClientShortcuts.Model
{
    [Table("Servers")]
    public class Servers
    {
        //public int id { get; set; }
        [Key]
        public string ServidorId { get; set; }
        
        public string ServicioWindows { get; set; }
        public string RutaSRFServidor { get; set; }
        public string RutaSRFCompilacion { get; set; }
        public string RutaLogs { get; set; }
        public string RutaDAT { get; set; }
        public string RutaBINSiebelServer { get; set; }
        public string RutaInstSiebel { get; set; }
        public string RutaLocalTools { get; set; }
        public string MonitoreoArchivoLogENT { get; set; }
        public string EnterpriseSiebel { get; set; }
        public string CompilaCadena { get; set; }
        public string ConectionString { get; set; }
        public string Perfil { get; set; }
        public string Gateway { get; set; }
        public string DescripcionSrv { get; set; }
        public string RutaBrowsersScripts1 { get; set; }
        public string RutaBrowsersScripts2 { get; set; }
        public string ODBC { get; set; }
        public string PasswordSADMIN { get; set; }
        public string PasswordSIEBEL { get; set; }
        public string RutaTools { get; set; }
        public string RutaSchema { get; set; }
        public string PasswordURL { get; set; }
        public string Activo { get; set; }
        public string Debug { get; set; }
    }
}