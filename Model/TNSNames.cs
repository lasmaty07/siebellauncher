﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace siebelClientShortcuts.Model
{
    [Table("TNSNames")]
    public class TNSNames
    {
        [Key]
        public string Ambiente { get; set; }
        public string Alias { get; set; }
    }
}
