﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace siebelClientShortcuts.Model
{
    [Table("ConfigSiebel")]
    public class ConfigSiebel
    {
        [Key]
        public int Id { get; set; }
        public string LoginName { get; set; }
        public string Envirorment { get; set; }
        public string Browser { get; set; }
        public string Application { get; set; }
        public string DebugMode { get; set; }
        public string SQLTrace { get; set; }
        public string SQLTracePath { get; set; }
        public string SiebelInstallationPath { get; set; }
        public string SRFLocalPath { get; set; }
        public string DesaName { get; set; }
        public string InteName { get; set; }
        public string HomoName { get; set; }
    }
}
