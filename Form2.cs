﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace siebelClientShortcuts
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            this.CenterToScreen();           

            using (var db = new Model.MyContext())
            {
                var config = db.ConfigSiebel.Find(1);
                var desa = db.TNSNames.Where(s => s.Ambiente == "desarrollo")
                    .First();
                var inte = db.TNSNames.Where(s => s.Ambiente == "integracion")
                    .First();
                var homo = db.TNSNames.Where(s => s.Ambiente == "homologacion")
                    .First();
                var prod = db.TNSNames.Where(s => s.Ambiente == "produccion")
                    .First();
                this.textBox1.Text = config.SiebelInstallationPath;
                this.textBox2.Text = config.SQLTracePath;
                this.textBox3.Text = config.SRFLocalPath;
                this.textBox4.Text = config.DesaName;
                this.textBox5.Text = config.InteName;
                this.textBox6.Text = config.HomoName;
                this.textBox7.Text = desa.Alias;
                this.textBox8.Text = inte.Alias;
                this.textBox9.Text = homo.Alias;
                this.textBox10.Text = prod.Alias;
            }
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (this.validatePath())
            {
                this.saveConfigFile();
                this.Close();
            }
            else
            {
                MessageBox.Show(string.Concat("No se pudo encontrar el Archivo Siebel.exe ", Environment.NewLine, "Recuerde colocar hasta la carpeta CLIENT"));
            }
        }

        private void saveConfigFile()
        {
            claseEncriptar descr = new claseEncriptar();
            commonMethods.logMessage(descr.Desencriptar("CvSe4lAYdwdIXuJ42HqgIA=="));
            commonMethods.logMessage(descr.Desencriptar("l35ZUJJGF+s6tNmruxEOrA=="));
            using (var db = new Model.MyContext())
            {
                var config = db.ConfigSiebel.Find(1);
                var desa = db.TNSNames.Where(s => s.Ambiente == "desarrollo")
                    .First();
                var inte = db.TNSNames.Where(s => s.Ambiente == "integracion")
                    .First();
                var homo = db.TNSNames.Where(s => s.Ambiente == "homologacion")
                    .First();
                var prod = db.TNSNames.Where(s => s.Ambiente == "produccion")
                    .First();
                config.SiebelInstallationPath = this.textBox1.Text ;                
                config.SQLTracePath = this.textBox2.Text;
                config.SRFLocalPath = this.textBox3.Text;
                config.DesaName = this.textBox4.Text;
                config.InteName = this.textBox5.Text;
                config.HomoName = this.textBox6.Text;
                desa.Alias = this.textBox7.Text;
                inte.Alias = this.textBox8.Text;
                homo.Alias = this.textBox9.Text;
                prod.Alias = this.textBox10.Text;
                db.SaveChanges();
            }
        }

        private bool validatePath()
        {
            bool flag = false;
            if (File.Exists(string.Concat(this.textBox1.Text, "\\BIN\\siebel.exe")))
            {
                flag = true;
            }
            return flag;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog1.SelectedPath))
            {
                this.textBox3.Text = folderBrowserDialog1.SelectedPath.ToString();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog1.SelectedPath))
            {
                this.textBox2.Text = folderBrowserDialog1.SelectedPath.ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog1.ShowDialog();

            if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(folderBrowserDialog1.SelectedPath))
            {
                this.textBox1.Text = folderBrowserDialog1.SelectedPath.ToString();
            }
        }

    }
}
