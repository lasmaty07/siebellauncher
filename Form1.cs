using System;
using System.Data;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Security.Principal;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
//using Oracle.DataAccess.Client;
//using Oracle.DataAccess.Types;

namespace siebelClientShortcuts
{

	public class Form1 : Form
	{
		private string siebelPath = string.Empty;
        private string SQLTracePath = string.Empty;
		private string DesaConnectionCfgName = string.Empty;
		private string InteConnectionCfgName = string.Empty;
        private string HomoConnectionCfgName = string.Empty;
		private string siebelServerPath = "\\\\dappsbl02\\d$\\Siebel\\ses\\siebsrvr\\";
        private string finscfgpath = "\\bin\\esn\\fins.cfg";
        private string loycfgpath = "\\bin\\esn\\loyaltyscw.cfg";
        private string toolscfgpath = "\\bin\\esn\\tools.cfg";        
        private bool ConfigChanged = false;
        private string logLevel = Environment.GetEnvironmentVariable("SIEBEL_LOG_EVENTS");
		private int fireCount = 0;
		public bool copiadoEnCurso = false;
		private IContainer components = null;
		private Button launchSiebel;
		private RadioButton desaRadioButton;
		private RadioButton inteRadioButton;
		private RadioButton ieRadioButton;
		private RadioButton chromeRadioButton;
		private Label label3;
        private TextBox userText;
		private CheckBox debugFlag;
		private GroupBox groupBox1;
		private GroupBox groupBox2;
		private GroupBox groupBox3;
		private RadioButton loyaltyRadioButton;
		private RadioButton finsRadioButton;
		private NotifyIcon notifyIcon1;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
		private Label label1;
		private CheckBox sqlFlag;
		private Button configButton;
		private Button bajarArchivosCustom;
		private Label AppVersionLabel;
        private Button matarSiebel;
        private Label label4;
        private Label label2;
        private RadioButton firefoxRadioButton;
        private CheckBox wsFlag;
        private Label label5;
        private RadioButton homoRadioButton;
        private Button copiarEndpoint;
        private TextBox log_events;
        private Button setLogEvents;

        private Button matarIE;
        private TabControl Tab;
        private TabPage tabPage1;
        private TabPage tabPage2;
        private Label label6;
        private Button syncSchema;
        private Button exportDAT;
        private Button fullcompileButton;
        private GroupBox groupBox4;
        private RadioButton ServerInte02;
        private RadioButton ServerDesa03;
        private RadioButton ServerDesa02;
        private TextBox textBox1;
        
        private static System.Text.StringBuilder m_sbText;
        private Button generarBrowserScr;

		private ProgressBar progressBar1;

		public Form1()
		{
			this.InitializeComponent();
		}

		private string applicationSelected()
		{
			string str = null;
			if (this.finsRadioButton.Checked)
			{
                str = string.Concat("/c ", this.siebelPath, this.finscfgpath);
			}
			else if (this.loyaltyRadioButton.Checked)
			{
				str = string.Concat("/c ", this.siebelPath, this.loycfgpath);
			}
			return str;
		}

		private string browserSelected()
		{
			string str = null;
			if (this.ieRadioButton.Checked)
			{
                str = " ";
			}
			else if (this.chromeRadioButton.Checked)
			{
				str = (!Environment.Is64BitOperatingSystem ? "/b \"C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe\" " : "/b \"C:\\Program Files (x86)\\Google\\Chrome\\Application\\chrome.exe\" ");
			}
            else if (this.firefoxRadioButton.Checked)
            {
                str = ("/b \"C:\\Program Files\\Mozilla Firefox\\firefox.exe\" ");
            }
			return str;
		}

		private void button1_Click(object sender, EventArgs e)
		{
            if (!this.validateSelection())
			{
				DialogCenteringService dialogCenteringService = new DialogCenteringService(this);
				try
				{
					MessageBox.Show("Debe seleccionar Ambiente - Navegador - Aplicación - Usuario");
				}
				finally
				{
					if (dialogCenteringService != null)
					{
						((IDisposable)dialogCenteringService).Dispose();
					}
				}                
            }
            else if (!this.validatePath())
            {
                DialogCenteringService dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    MessageBox.Show("La ruta de Instalacion no es valida, abrir el Config y modificarla");
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
            else
            {
                Process process = new Process();
                ProcessStartInfo processStartInfo = new ProcessStartInfo()
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "cmd.exe",
                    Arguments = string.Concat("/C ", this.finalCommand())
                };
                if (this.validateRunningProcesses())
                {
                    process.StartInfo = processStartInfo;
                    process.Start();
                    base.Hide();
                    this.notifyIcon1.BalloonTipText = "Hecho!";
                    this.notifyIcon1.ShowBalloonTip(10);
                    commonMethods.logMessage(string.Concat("Ejecutando comando: ", this.finalCommand()), null);
                }
            }
		}

		private void button2_Click(object sender, EventArgs e)
		{
			DialogCenteringService dialogCenteringService;
			string str;
			string fileName;
			string str1;
			string str2;
			DirectoryInfo directoryInfo;
			int i;
			string[] files;
			int j;
			System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
			string str3 = string.Concat(this.siebelPath, "\\PUBLIC\\esn\\23048\\SCRIPTS\\siebel\\custom");
			string str4 = string.Concat(this.siebelPath, "\\PUBLIC\\esn\\23048\\SCRIPTS\\3rdParty");
			string str5 = string.Concat(this.siebelPath, "\\PUBLIC\\esn\\FILES\\custom");
			string str6 = string.Concat(this.siebelPath, "\\PUBLIC\\esn\\FILES\\3rdParty\\themes");
			string str7 = string.Concat(this.siebelPath, "\\PUBLIC\\esn\\IMAGES\\custom");
            string str8 = string.Concat(this.siebelServerPath, "WEBMASTER\\siebel_build\\scripts\\siebel\\custom");
            string str9 = string.Concat(this.siebelServerPath, "WEBMASTER\\siebel_build\\scripts\\3rdParty");
            string str10 = string.Concat(this.siebelServerPath, "WEBMASTER\\files\\esn\\custom");
            string str11 = string.Concat(this.siebelServerPath, "WEBMASTER\\files\\esn\\3rdParty\\themes");
            string str12 = string.Concat(this.siebelServerPath, "WEBMASTER\\images\\esn\\custom");
            commonMethods.logMessage("Copiando Archivos Custom", null);
            if (Directory.Exists(this.siebelPath) && this.validatePath())
			{
				if (!Directory.Exists(str3))
				{
					Directory.CreateDirectory(str3);
				}
				string[] directories = Directory.GetFiles(str3);                
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					(new FileInfo(str)).IsReadOnly = false;
				}
				directories = Directory.GetFiles(str8);
                commonMethods.logMessage(string.Concat("Destino: ", str3), null);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					fileName = Path.GetFileName(str);
                    commonMethods.logMessage(string.Concat("    Origen: ", str), null);                    
					this.CopyFileProc(str, Path.Combine(str3, fileName), false);
				}
				if (!Directory.Exists(str4))
				{
					Directory.CreateDirectory(str4);
				}
				directories = Directory.GetFiles(str4);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					(new FileInfo(str)).IsReadOnly = false;
				}
				directories = Directory.GetFiles(str9);
                commonMethods.logMessage(string.Concat("Destino: ", str4), null);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					fileName = Path.GetFileName(str);
                    commonMethods.logMessage(string.Concat("    Origen: ", str), null);
					this.CopyFileProc(str, Path.Combine(str4, fileName), false);
				}
				directories = Directory.GetDirectories(str9);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str2 = directories[i];
					directoryInfo = Directory.CreateDirectory(str2.Replace(str9, str4));
					files = Directory.GetFiles(str2);
                    commonMethods.logMessage(string.Concat("Destino: ", directoryInfo.FullName), null);
					for (j = 0; j < (int)files.Length; j++)
					{
						str = files[j];
						fileName = Path.GetFileName(str);
                        commonMethods.logMessage(string.Concat("    Origen: ", str), null);    
						str1 = Path.Combine(directoryInfo.FullName, fileName);
						this.CopyFileProc(str, str1, false);
					}
				}
				if (!Directory.Exists(str5))
				{
					Directory.CreateDirectory(str5);
				}
				directories = Directory.GetFiles(str5);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					(new FileInfo(str)).IsReadOnly = false;
				}
				directories = Directory.GetFiles(str10);
                commonMethods.logMessage(string.Concat("Destino: ", str5), null);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					fileName = Path.GetFileName(str);
                    commonMethods.logMessage(string.Concat("    Origen: ", str), null);    
					this.CopyFileProc(str, Path.Combine(str5, fileName), false);
				}
				if (!Directory.Exists(str6))
				{
					Directory.CreateDirectory(str6);
				}
				directories = Directory.GetFiles(str6);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					(new FileInfo(str)).IsReadOnly = false;
				}
				directories = Directory.GetFiles(str11);
                commonMethods.logMessage(string.Concat("Destino: ", str6), null);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					fileName = Path.GetFileName(str);
                    commonMethods.logMessage(string.Concat("    Origen: ", str), null);           
					this.CopyFileProc(str, Path.Combine(str6, fileName), false);
				}
				directories = Directory.GetDirectories(str11);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str2 = directories[i];
					directoryInfo = Directory.CreateDirectory(str2.Replace(str11, str6));
					files = Directory.GetFiles(str2);
					for (j = 0; j < (int)files.Length; j++)
					{
						str = files[j];
						fileName = Path.GetFileName(str);
						str1 = Path.Combine(directoryInfo.FullName, fileName);
						this.CopyFileProc(str, str1, false);
					}
				}
				if (!Directory.Exists(str7))
				{
					Directory.CreateDirectory(str7);
				}
				directories = Directory.GetFiles(str7);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					(new FileInfo(str)).IsReadOnly = false;
				}
				directories = Directory.GetFiles(str12);
                commonMethods.logMessage(string.Concat("Destino: ", str7), null);
				for (i = 0; i < (int)directories.Length; i++)
				{
					str = directories[i];
					fileName = Path.GetFileName(str);
                    commonMethods.logMessage(string.Concat("    Origen: ", str), null);
					this.CopyFileProc(str, Path.Combine(str7, fileName), false);
				}
				System.Windows.Forms.Cursor.Current = Cursors.Default;
				if (!this.copiadoEnCurso)
				{
					dialogCenteringService = new DialogCenteringService(this);
					try
					{
                        this.progressBar1.Visible = false;
                        commonMethods.logMessage("Archivos copiados correctamente", null);
						MessageBox.Show("Archivos copiados correctamente");
					}
					finally
					{
						if (dialogCenteringService != null)
						{
							((IDisposable)dialogCenteringService).Dispose();
						}
					}
				}                
			}
			else
			{
				commonMethods.logMessage(string.Concat("No existe el directorio: ", this.siebelPath), null);
				dialogCenteringService = new DialogCenteringService(this);
				try
				{
					MessageBox.Show("Path de instalación de Siebel incorrecto. Revisar la Configuración");
				}
				finally
				{
					if (dialogCenteringService != null)
					{
						((IDisposable)dialogCenteringService).Dispose();
					}
				}
			}
		}

		private void ChildClick(object sender, EventArgs e)
		{
		}

		private void configLocationButton_Click(object sender, EventArgs e)
		{
            this.saveConfigFile();
            Form2 f2 = new Form2();
            f2.Show();
		}

		private void configWatcher()
		{
			FileSystemWatcher fileSystemWatcher = new FileSystemWatcher()
			{
				Path = string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "\\siebelLauncher"),
				Filter = "Config.xml"
			};
			fileSystemWatcher.Changed += new FileSystemEventHandler(this.OnConfigChanged);
			fileSystemWatcher.EnableRaisingEvents = true;
		}

        private void abrirConsolaSiebel(object sender, EventArgs e)
        {
            int num = sender.ToString().IndexOf(":") + 1;
            string str = sender.ToString().Substring(num);
            string innerXml = commonMethods.config.SelectSingleNode(string.Concat("//", str)).InnerXml;
            string loginName = this.userText.Text;
            string enterprise = commonMethods.config.SelectSingleNode(string.Concat("//Enterprise//", str)).InnerXml;
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = string.Concat("\\\\", innerXml, "\\d$\\Siebel\\ses\\siebsrvr\\BIN\\srvrmgr.exe");
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "/U " + loginName + " /P " + loginName + "  /G " + innerXml + " /E " + enterprise + " /S " + innerXml;
            try
            {
                Process.Start(startInfo);
            }
            catch (Exception exception)
            {
                commonMethods.logMessage(string.Concat("Error al abrir Consola Siebel: ", str), exception.InnerException);
            }
            commonMethods.logMessage(string.Concat("Abrir consola Siebel: ", str), null);
        }

        private void abrirConsolaSQL(object sender, EventArgs e)
        {            
            int num = sender.ToString().IndexOf(":") + 1;
            string str = sender.ToString().Substring(num);
            string innerXml = commonMethods.config.SelectSingleNode(string.Concat("//TNSNames//", str)).InnerXml;
            string loginName = this.userText.Text;
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.UseShellExecute = false;
            startInfo.FileName = "SQLPLUS";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            string password = loginName;
            string str2 = string.Concat(loginName, "/", loginName + "@", innerXml);
            if (str == "Produccion")
            {
                ShowInputDialog(ref password);
                str2 = string.Concat(loginName, "/", password + "@", innerXml);
            }            
            startInfo.Arguments = str2;
            commonMethods.logMessage(string.Concat("Iniciando Consola Siebel con parametros: ", str2), null);
            try
            {
                Process.Start(startInfo);
            }
            catch (Exception exception)
            {
                commonMethods.logMessage("Error al abrir Consola Siebel ", exception.InnerException);
            }
        }        

        private void abrirCFG(object sender, EventArgs e) 
        {
            commonMethods.logMessage("Abrir cfg ", null);
            string runcom = null;            
            if (sender.ToString()=="Fins") {
                runcom = string.Concat(this.siebelPath, this.finscfgpath);
            }
            else if (sender.ToString() == "Loyalty Portal")
            {
                runcom = string.Concat(this.siebelPath, this.loycfgpath);
            }
            else
            {
                runcom = string.Concat(this.siebelPath, this.toolscfgpath);
            }            
            try
            {
                Process.Start(runcom);
            }
            catch (Exception exception)
            {
                commonMethods.logMessage(string.Concat("Error al abrir CFG: ", runcom), exception.InnerException);
            }

        }

      
        private void abrirCarpeta(object sender, EventArgs e)
        {

            commonMethods.logMessage("Abrir carpeta", null);
            try
            {
                //Process.Start();
                commonMethods.logMessage("Nothing happens ", null);
            }
            catch (Exception exception)
            {
                commonMethods.logMessage("Error al abrir la carpeta de logs Local ", exception.InnerException);
            }
        }

        private void abrirlog(object sender, EventArgs e)
        {
            string envVar = Environment.GetEnvironmentVariable("SIEBEL_LOG_DIR");
            commonMethods.logMessage(string.Concat("Abrir carpeta Logs: ",envVar), null);
            try
            {
                Process.Start(envVar);
            }
            catch (Exception exception)
            {
                commonMethods.logMessage("Error al abrir la carpeta de logs Local ", exception.InnerException);
            }
        }

        private void borrarLog(object sender, EventArgs e)
        {
            string envVar = Environment.GetEnvironmentVariable("SIEBEL_LOG_DIR");
            DialogCenteringService dialogCenteringService = new DialogCenteringService(this);
            if (MessageBox.Show(string.Concat("Se borraran todos los archivo dentro de la carpeta: ", envVar), "Borrar Logs", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
                try
                {                   
                    System.IO.DirectoryInfo di = new DirectoryInfo(envVar);
                    commonMethods.logMessage("borrar archivos de Log", null);
                    int locked = 0;
                    int deleted = 0;
                    foreach (FileInfo file in di.GetFiles())
                    {
                        if (!this.IsFileLocked(file))
                        {
                            file.Delete();
                            deleted++;
                        }
                        else
                        {
                            locked++;
                        }
                    }
                    commonMethods.logMessage(string.Concat("borrar archivos de Log, borrados:", deleted, " bloqueados:" ,locked), null);
                    MessageBox.Show(string.Concat("Se borraron ", deleted, " archivos, y ", locked, " archivos se encuentran en uso"));
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage("Error al borrar los archivos.", exception.InnerException);
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
        }

        protected virtual bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }
            //file is not locked
            return false;
        }

        private void loginsEnSiebel(object sender, EventArgs e)
        {
            /* System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            int num = sender.ToString().IndexOf(":") + 1;
            string str = sender.ToString().Substring(num);
            string innerXml = commonMethods.config.SelectSingleNode(string.Concat("//", str)).InnerXml;
            string user = this.userText.Text;            
            string enterprise = commonMethods.config.SelectSingleNode(string.Concat("//Enterprise//", str)).InnerXml;
            commonMethods.logMessage(string.Concat("Consultando Logins en Siebel ", str, user, enterprise));
            Process p = new Process();
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = "\\\\iappsbl02\\d$\\Siebel\\ses\\siebsrvr\\BIN\\srvrmgr.exe";
            p.StartInfo.Arguments = string.Concat(" /U ", user, " /P ", user, " /G ", innerXml, " /E ", enterprise, " /t 1 /c \"list active tasks for comp FinsObjMgr_esn\" ");
            p.StartInfo.CreateNoWindow = true;
            try
            {
                commonMethods.logMessage(string.Concat("Ejecutando consulta usuarios ", p.StartInfo.FileName, " ", p.StartInfo.Arguments));
                p.Start();
            }
            catch (Exception exception)
            {
                commonMethods.logMessage("Error al consultar usuarios:  ", exception.InnerException);
            }            

            for (var i = 0; i < 26; i++)
            {
                p.StandardOutput.ReadLine();
            }

            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();

            char[] delimiters = new char[] { '\r', '\n' };
            string[] arr = output.Split(delimiters,StringSplitOptions.RemoveEmptyEntries);

            List<string> show = new List<string>();
            List<string> query = new List<string>();
            show.Add(Environment.NewLine);
            query.Add(string.Concat("SELECT U.LOGIN,CON.LAST_NAME APELLIDO,CON.FST_NAME NOMBRE FROM SIEBEL.S_USER U INNER JOIN SIEBEL.S_CONTACT CON ON CON.ROW_ID = U.ROW_ID WHERE U.LOGIN IN( "));
            foreach (var substring in arr) 
            {
                if (substring.Length > 180)
                {
                    show.Add(string.Concat(" ",substring.Substring(173, 8),Environment.NewLine));
                    query.Add(string.Concat(" '", substring.Substring(173, 8),"',"));
                }
            }           

            string queryfinal = string.Concat(query);
            queryfinal = queryfinal.Remove(queryfinal.Length - 1);
            queryfinal = string.Concat(queryfinal, ")");

            string loginsfinal = string.Concat(show);

            string oradb = "Data Source=(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = exa01-scan.bancogalicia.com.ar)(PORT = 1521))(CONNECT_DATA =(SERVER = DEDICATED)(SERVICE_NAME = SIEBELD2)));Password=L0661007;User ID=L0661007";

            commonMethods.logMessage("Estableciendo conexion con la base de datos");
            try
            {
                OracleConnection conn = new Oracle.DataAccess.Client.OracleConnection(oradb);
                conn.Open();
                OracleCommand cmd = new OracleCommand(queryfinal, conn);
                cmd.Connection = conn;
                OracleDataReader dr = cmd.ExecuteReader();
                dr.Read();
                DataTable dt = new DataTable();
                dt.Load(dr);
                dataGridView1.DataSource = dt;
                conn.Dispose();
                commonMethods.logMessage("Conexion establecida y consulta ejecutada");

            }

            
            System.Drawing.Size size = new System.Drawing.Size(50, 50);
            Form inputBox = new Form();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.AutoSize = true;
            inputBox.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            inputBox.Text = "Usuarios Conectados";

            Label texto1 = new Label();
            texto1.Name = "Usuarios";
            texto1.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            //texto1.Text = loginsfinal;
            texto1.Text = loginsfinal;
            texto1.AutoSize = true;
            texto1.TextAlign = ContentAlignment.MiddleCenter;
            inputBox.Controls.Add(texto1);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(texto1.Width + 10 , texto1.Height + 10 );
            inputBox.Controls.Add(okButton);            

            inputBox.AcceptButton = okButton;
            inputBox.StartPosition = FormStartPosition.CenterParent;

            this.notifyIcon1.BalloonTipText = "Consulta copiada al portapapeles";
            this.notifyIcon1.ShowBalloonTip(15); 

            //Clipboard.SetText(queryfinal);

            DialogResult result = inputBox.ShowDialog();
            label6.Text = string.Concat("Usuarios conectados en :",innerXml);
            commonMethods.logMessage(string.Concat("Usuarios conectados: ", loginsfinal ));
            
            System.Windows.Forms.Cursor.Current = Cursors.Default;           */
            MessageBox.Show("se desactiva hasta no quitar los bugs");
        }

        private void fullCompile(object sender, EventArgs e)
        {
            string str = string.Concat(Environment.GetEnvironmentVariable("TNS_ADMIN"), "\\tnsnames.ora");
            commonMethods.logMessage("Abrir archivo TNS ", null);
            if (Environment.GetEnvironmentVariable("TNS_ADMIN") != "")
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = @"notepad++.exe";
                startInfo.Arguments = str;
                try
                {
                    Process.Start(startInfo);
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage("Error al abrir el TNS Names ", exception.InnerException);
                }
            }
            else
            {
                commonMethods.logMessage("No se encontro la variable de entorno TNS_ADMIN ");
                DialogCenteringService dialogCenteringService;
                dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    MessageBox.Show("No se pudo encontrar el Archivo TNS Names. Error en la variable TNS_ADMIN");
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
        }

        private void abrirTNS(object sender, EventArgs e)
        {            
            string str = string.Concat(Environment.GetEnvironmentVariable("TNS_ADMIN"), "\\tnsnames.ora");
            commonMethods.logMessage("Abrir archivo TNS ", null);
            if (Environment.GetEnvironmentVariable("TNS_ADMIN") != "" )
            {
                ProcessStartInfo startInfo = new ProcessStartInfo();
                startInfo.FileName = @"notepad++.exe";
                startInfo.Arguments = str;
                try
                {
                    Process.Start(startInfo);
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage("Error al abrir el TNS Names ", exception.InnerException);
                }
            }
            else 
            {
                commonMethods.logMessage("No se encontro la variable de entorno TNS_ADMIN ");
                DialogCenteringService dialogCenteringService;
                dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    MessageBox.Show("No se pudo encontrar el Archivo TNS Names. Error en la variable TNS_ADMIN");
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
        }

        private void enviarSRF(object sender, EventArgs e)
        {
            DialogCenteringService dialogCenteringService;
            commonMethods.logMessage("Copiado de SRF", null);
            int num = sender.ToString().IndexOf(":") + 1;
            string str = sender.ToString().Substring(num);
            string innerXml = commonMethods.config.SelectSingleNode(string.Concat("//", str)).InnerXml;
            string innerXml1 = commonMethods.config.SelectSingleNode("//SRFLocalPath").InnerXml;
            System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
            string str1 = string.Concat(innerXml1, "\\siebel_sia.srf");
            string str2 = string.Concat("\\\\", innerXml, "\\SRF\\siebel_sia.srf");
            if ((!File.Exists(str1) ? true : !Directory.Exists(innerXml1)))
            {
                commonMethods.logMessage(string.Concat("Archivo origen: ", str1), null);
                commonMethods.logMessage(string.Concat("Destino: ", str2), null);
                commonMethods.logMessage("Archivo no copiado. Rutas inválidas", null);
                dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    MessageBox.Show("El nombre del servidor o la ruta local no son correctas, revisar .config");
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
            else
            {
                if (!File.Exists(str2))
                {
                    this.CopyFileProc(str1, str2, true);
                }
                else
                {
                    dialogCenteringService = new DialogCenteringService(this);
                    try
                    {
                        if (MessageBox.Show("Existe un .SRF en el Server, ¿Desea reemplazarlo?", "SRF Existente", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            this.CopyFileProc(str1, str2, true);
                            commonMethods.logMessage("Reemplazar SRF existente", null);
                        }
                    }
                    finally
                    {
                        if (dialogCenteringService != null)
                        {
                            ((IDisposable)dialogCenteringService).Dispose();
                        }
                    }
                }
                commonMethods.logMessage("SRF Copiado OK", null);
                commonMethods.logMessage(string.Concat("Origen:", str1), null);
                commonMethods.logMessage(string.Concat("Destino:", str2), null);
            }
            System.Windows.Forms.Cursor.Current = Cursors.Default;
        }

		private void copiarSRF(object sender, EventArgs e)
		{
			DialogCenteringService dialogCenteringService;
			commonMethods.logMessage("Copiado de SRF", null);
			int num = sender.ToString().IndexOf(":") + 1;
			string str = sender.ToString().Substring(num);
			string innerXml = commonMethods.config.SelectSingleNode(string.Concat("//", str)).InnerXml;
			string innerXml1 = commonMethods.config.SelectSingleNode("//SRFLocalPath").InnerXml;
			System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
			string str1 = string.Concat("\\\\", innerXml, "\\d$\\Siebel\\ses\\siebsrvr\\OBJECTS\\esn\\siebel_sia.srf");
			string str2 = string.Concat(innerXml1, "\\siebel_sia.srf");
			if ((!File.Exists(str1) ? true : !Directory.Exists(innerXml1)))
			{
				commonMethods.logMessage(string.Concat("Archivo origen: ", str1), null);
				commonMethods.logMessage(string.Concat("Destino: ", innerXml1), null);
				commonMethods.logMessage("Archivo no copiado. Rutas inválidas", null);
				dialogCenteringService = new DialogCenteringService(this);
				try
				{
					MessageBox.Show("El nombre del servidor o la ruta local no son correctas, revisar .config");
				}
				finally
				{
					if (dialogCenteringService != null)
					{
						((IDisposable)dialogCenteringService).Dispose();
					}
				}
			}
			else
			{
				if (!File.Exists(string.Concat(innerXml1, "\\siebel_sia.srf")))
				{
					this.CopyFileProc(str1, str2, true);
				}
				else
				{
					dialogCenteringService = new DialogCenteringService(this);
					try
					{
						if (MessageBox.Show("Existe un .SRF local, ¿Desea reemplazarlo?", "SRF Existente", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
						{
                            commonMethods.logMessage("Reemplazar SRF existente", null);
                            FileInfo file = new FileInfo(string.Concat(innerXml1, "\\siebel_sia.srf"));
                            if (this.IsFileLocked(file))
                            {
                                if (MessageBox.Show("El archivo se encuentra bloqueado ¿Desea Matar siebel.exe?", "Archivo bloqueado", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    Process[] processesByName = Process.GetProcessesByName("siebel");
                                    Process[] processArray;
                                    Process process;
                                    processArray = processesByName;
                                    for (var i = 0; i < (int)processArray.Length; i++)
                                    {
                                        process = processArray[i];
                                        if (!process.HasExited)
                                        {
                                            process.Kill();
                                            commonMethods.logMessage(string.Concat("Matando Siebel.exe process ID ", process.Id), null);
                                        }
                                    }
                                    this.CopyFileProc(str1, str2, true);
                                }

                            }
                            else
                            {
                                this.CopyFileProc(str1, str2, true);
                            }
						}
					}
                    catch (Exception exception)
                    {
                        commonMethods.logMessage("Error al copiar el SRF : ", exception.InnerException);
                    }
					finally
					{
						if (dialogCenteringService != null)
						{
							((IDisposable)dialogCenteringService).Dispose();
						}
					}
				}
				commonMethods.logMessage("SRF Copiado OK", null);
				commonMethods.logMessage(string.Concat("Origen:", str1), null);
				commonMethods.logMessage(string.Concat("Destino:", str2), null);
			}
			System.Windows.Forms.Cursor.Current = Cursors.Default;
		}

		private void CopyFileProc(string sourceFilePath, string destinationFilePath, bool showOkMessage = true)
		{
			this.progressBar1.Visible = true;
			this.progressBar1.Value = 0;
			WebClient webClient = new WebClient();
			webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(this.wc_DownloadProgressChanged);
			if (showOkMessage)
			{
				webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(this.wc_DownloadFileCompleted);
			}
			webClient.DownloadFileAsync(new Uri(sourceFilePath), destinationFilePath);
		}

		public void createIconMenuStructure()
		{
			ToolStripMenuItem toolStripMenuItem = new ToolStripMenuItem()
			{
				Text = "Salir"
			};
			toolStripMenuItem.Click += new EventHandler(this.exitButtonMenu);
			this.contextMenu.Items.Add(toolStripMenuItem);
            
		}

        private void createMenuAndItems()
        {
            string str;
            ToolStripMenuItem toolStripMenuItem;
            int i;
            base.Invoke(new Action(() =>
            {
                base.Controls.Find("topMenu", false);
                if (base.Controls.IndexOfKey("topMenu") > 0)
                {
                    base.Controls.RemoveByKey("topMenu");
                }
            }));
            MenuStrip menuStrip = new MenuStrip()
            {
                Name = "topMenu"
            };
            base.Invoke(new Action(() => this.Controls.Add(menuStrip)));
            string[] strArrays = new string[] { "Menú", "Consola", "Ayuda" };
            string[] array = strArrays;
            for (i = 0; i < (int)array.Length; i++)
            {
                ToolStripMenuItem toolStripMenuItem1 = new ToolStripMenuItem(array[i]);
                menuStrip.Items.Add(toolStripMenuItem1);               
            }

            foreach (ToolStripMenuItem item in menuStrip.Items)
            {
                if (item.Text == "Menú")
                {
                    strArrays = new string[] { "Copiado SRF a local", "Copiado SRF a Server" , "Abrir .cfg", "Logs", "Salir" };
                    array = strArrays;                    
                    for (i = 0; i < (int)array.Length; i++)
                    {
                        str = array[i];
                        toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.ChildClick));
                        item.DropDownItems.Add(toolStripMenuItem);
                    }                    
                    foreach (ToolStripMenuItem dropDownItem in item.DropDownItems)
                    {
                        if (dropDownItem.Text == "Copiado SRF a local")
                        {
                            array = this.fetchServersFromConfig().ToArray();
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                toolStripMenuItem = new ToolStripMenuItem(string.Concat("Origen: ", str), null, new EventHandler(this.copiarSRF));
                                dropDownItem.DropDownItems.Add(toolStripMenuItem);
                            }
                        }
                        if (dropDownItem.Text == "Copiado SRF a Server")
                        {
                            array = this.fetchServersFromConfig().ToArray();
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                toolStripMenuItem = new ToolStripMenuItem(string.Concat("Destino: ", str), null, new EventHandler(this.enviarSRF));
                                dropDownItem.DropDownItems.Add(toolStripMenuItem);
                            }
                        }
                        if (dropDownItem.Text == "Abrir .cfg")
                        {
                            array = new string[] { "Fins", "Loyalty Portal","Tools.cfg" };
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirCFG));
                                dropDownItem.DropDownItems.Add(toolStripMenuItem);
                            }
                        }
                        if (dropDownItem.Text == "Logs")
                        {
                            array = new string[] { "Abrir Carpeta Log","Borrar Archivos de Log" };
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                if (str == "Abrir Carpeta Log")
                                {                                    
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirlog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "Borrar Archivos de Log")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.borrarLog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                            }
                        }
                        if (dropDownItem.Text == "Salir")
                        {
                            dropDownItem.ShortcutKeys = Keys.Control | Keys.Q;
                            dropDownItem.Click += new EventHandler(this.exitButtonMenu);
                        }
                    }
                }
                if (item.Text == "Consola")
                {
                    strArrays = new string[] { "Consola Siebel", "SQL Plus", "Usuarios Conectados", "Abrir TNS Names" };
                    array = strArrays;
                    for (i = 0; i < (int)array.Length; i++)
                    {
                        str = array[i];
                        toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.ChildClick));
                        item.DropDownItems.Add(toolStripMenuItem);
                    }
                    foreach (ToolStripMenuItem dropDownItem in item.DropDownItems)
                    {
                        if (dropDownItem.Text == "Consola Siebel")
                        {
                            array = this.fetchServersFromConfig().ToArray();
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirConsolaSiebel));
                                dropDownItem.DropDownItems.Add(toolStripMenuItem);
                            }
                        }
                        if (dropDownItem.Text == "SQL Plus")
                        {
                            array = this.fetchTNSFromConfig().ToArray();
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirConsolaSQL));
                                dropDownItem.DropDownItems.Add(toolStripMenuItem);
                            }
                        }
                        if (dropDownItem.Text == "Usuarios Conectados")
                        {
                            array = this.fetchServersFromConfig().ToArray();
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.loginsEnSiebel));
                                dropDownItem.DropDownItems.Add(toolStripMenuItem);
                            }                            
                        }
                        if (dropDownItem.Text == "Abrir TNS Names")
                        {
                            dropDownItem.Click += new EventHandler(this.abrirTNS);
                        }
                    }
                }
                if (item.Text == "Carpetas")
                {
                    strArrays = this.fetchServersFromConfig().ToArray();
                    //strArrays = new string[] { "Desarrollo", "Integración", "Homologación", "Produccion" };
                    array = strArrays;
                    for (i = 0; i < (int)array.Length; i++)
                    {
                        str = array[i];
                        toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.ChildClick));
                        item.DropDownItems.Add(toolStripMenuItem);
                    }
                    foreach (ToolStripMenuItem dropDownItem in item.DropDownItems)
                    {
                        if (dropDownItem.Text == "Desarrollo")
                        {
                            array = new string[] { "Logs", "SRF","DAT" };
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                if (str == "Logs")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirCarpeta));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "SRF")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirCarpeta));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "DAT")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirCarpeta));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                            }
                        }
                        if (dropDownItem.Text == "Integración")
                        {
                            array = new string[] { "Logs", "SRF", "DAT" };
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                if (str == "Logs")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirlog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "SRF")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.borrarLog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "DAT")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.borrarLog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                            }
                        }
                        if (dropDownItem.Text == "Homologación")
                        {
                            array = new string[] { "Logs", "SRF", "DAT" };
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                if (str == "Logs")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirlog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "SRF")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.borrarLog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "DAT")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.borrarLog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                            }
                        }
                        if (dropDownItem.Text == "Produccion")
                        {
                            array = new string[] { "Logs", "SRF", "DAT" };
                            for (i = 0; i < (int)array.Length; i++)
                            {
                                str = array[i];
                                if (str == "Logs")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.abrirlog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "SRF")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.borrarLog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                                if (str == "DAT")
                                {
                                    toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.borrarLog));
                                    dropDownItem.DropDownItems.Add(toolStripMenuItem);
                                }
                            }
                        }
                    }
                }
                if (item.Text == "Ayuda")
                {
                    strArrays = new string[] { "Acerca De", "Logs App"  };
                    array = strArrays;
                    for (i = 0; i < (int)array.Length; i++)
                    {
                        str = array[i];
                        toolStripMenuItem = new ToolStripMenuItem(str, null, new EventHandler(this.ChildClick));
                        item.DropDownItems.Add(toolStripMenuItem);
                    }
                    foreach (ToolStripMenuItem dropDownItem in item.DropDownItems)
                    {
                        if (dropDownItem.Text == "Acerca De")
                        {
                            dropDownItem.Click += new EventHandler(this.showAboutApp);
                        }
                        if (dropDownItem.Text == "Logs App")
                        {
                            dropDownItem.Click += new EventHandler(this.openLogFile);
                        }                        
                    }
                }
            }
        }

		private string debugSelected()
		{
			return (this.debugFlag.Checked ? "/h" : "");
		}

		protected override void Dispose(bool disposing)
		{
			if ((!disposing ? false : this.components != null))
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		private string envirormentSelected()
		{
			string str = null;
			if (this.desaRadioButton.Checked)
			{
				str = string.Concat("/d ", this.DesaConnectionCfgName);
			}
			else if (this.inteRadioButton.Checked)
			{
				str = string.Concat("/d ", this.InteConnectionCfgName);
			}
            else if (this.homoRadioButton.Checked)
            {
                str = string.Concat("/d ", this.HomoConnectionCfgName);
            }
			return str;
		}        

		private void exitButtonMenu(object sender, EventArgs e)
		{
			DateTime now = DateTime.Now;
            try
            {
                commonMethods.logMessage(string.Concat("Grabando archivo de Config ", now.ToString()), null);
                this.saveConfigFile();
            }
            catch (Exception exception)
            {
                commonMethods.logMessage("Error al grabar el .config: ", exception.InnerException);
            }
			commonMethods.logMessage(string.Concat("Cerrando App, Fecha: ", now.ToString()), null);
			commonMethods.logMessage("--------------------------------", null);
			this.notifyIcon1.Dispose();
			Environment.Exit(0);
		}


        private void saveConfigFile()
        {
            commonMethods.config.SelectSingleNode("Parameters/LoginName").InnerXml = this.userText.Text;
            commonMethods.config.SelectSingleNode("Parameters/Envirorment").InnerXml = (this.desaRadioButton.Checked ? "DESA" : (this.inteRadioButton.Checked ? "INTE" : "HOMO"));
            commonMethods.config.SelectSingleNode("Parameters/Browser").InnerXml = (this.ieRadioButton.Checked ? "IE" : (this.chromeRadioButton.Checked ? "CHROME" : "FIREFOX"));
            commonMethods.config.SelectSingleNode("Parameters/Application").InnerXml = (this.finsRadioButton.Checked ? "FINS" : "PORTAL");
            commonMethods.config.SelectSingleNode("Parameters/DebugMode").InnerXml = (this.debugFlag.Checked ? "TRUE" : "FALSE");
            commonMethods.config.SelectSingleNode("Parameters/SQLTrace").InnerXml = (this.sqlFlag.Checked ? "TRUE" : "FALSE");
            commonMethods.config.Save(commonMethods.configFile);
        }

        private void checkConfigVersion()
        {
            string vers = (commonMethods.config.SelectSingleNode("Parameters/AppVersion") != null ? commonMethods.config.SelectSingleNode("Parameters/AppVersion").InnerXml : "");
            string intallPath = (commonMethods.config.SelectSingleNode("Parameters/SiebelInstallationPath") != null ? commonMethods.config.SelectSingleNode("Parameters/SiebelInstallationPath").InnerXml : "");
            string SRFLocalPath = (commonMethods.config.SelectSingleNode("Parameters/CFGConfig/SRFLocalPath") != null ? commonMethods.config.SelectSingleNode("Parameters/CFGConfig/SRFLocalPath").InnerXml : "");
            string SQLTracePath = (commonMethods.config.SelectSingleNode("Parameters/SQLTracePath") != null ? commonMethods.config.SelectSingleNode("Parameters/SQLTracePath").InnerXml : "");
            string DesaName = (commonMethods.config.SelectSingleNode("Parameters/CFGConfig/DesaName") != null ? commonMethods.config.SelectSingleNode("Parameters/CFGConfig/DesaName").InnerXml : "");
            string InteName = (commonMethods.config.SelectSingleNode("Parameters/CFGConfig/InteName") != null ? commonMethods.config.SelectSingleNode("Parameters/CFGConfig/InteName").InnerXml : "");
            if (vers != commonMethods.AppVersion)
            {
                string path = Path.Combine(string.Concat(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "\\siebelLauncher\\"));
                try
                {
                    File.Copy(commonMethods.configFile, Path.Combine(path, "config_old.xml"), true);
                    File.Delete(commonMethods.configFile);
                    commonMethods.createXMLFile(intallPath, SRFLocalPath, SQLTracePath, DesaName, InteName);
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage("Error al borrar el .config: ", exception.InnerException);   
                }
                commonMethods.logMessage(string.Concat("Version del archivo config no corresponde a esta version, genarando old y nuevo .config :", DateTime.Now.ToString()), null);
                this.notifyIcon1.BalloonTipText = "Archivo de Configuracion Modificado! Abrir el config nuevamente.";
                this.notifyIcon1.ShowBalloonTip(100);
                ConfigChanged = true;
            }
        }        

		private void fetchConfigFromFile()
		{
			bool flag;
			bool flag1;
			try
			{
                string str = (commonMethods.config.SelectSingleNode("Parameters/LoginName") != null ? commonMethods.config.SelectSingleNode("Parameters/LoginName").InnerXml : "");
				if (!this.userText.InvokeRequired)
				{
					this.userText.Text = str;
				}
				else
				{
					this.userText.Invoke(new Action(() => this.userText.Text = str));
				}
				string str1 = (commonMethods.config.SelectSingleNode("Parameters/Envirorment") != null ? commonMethods.config.SelectSingleNode("Parameters/Envirorment").InnerXml : "");
				if (str1 == "DESA")
				{
					this.desaRadioButton.Checked = true;
				}
				else if (str1 == "INTE")
				{
					this.inteRadioButton.Checked = true;
                }
                else if (str1 == "HOMO")
                {
                    this.homoRadioButton.Checked = true;
                }
				string str2 = (commonMethods.config.SelectSingleNode("Parameters/Browser") != null ? commonMethods.config.SelectSingleNode("Parameters/Browser").InnerXml : "");
				if (str2 == "IE")
				{
					this.ieRadioButton.Checked = true;
				}
				else if (str2 == "CHROME")
				{
					this.chromeRadioButton.Checked = true;
				}
				else if (str2 == "FIREFOX")
				{
                    this.firefoxRadioButton.Checked = true;
				}
				string str3 = (commonMethods.config.SelectSingleNode("Parameters/Application") != null ? commonMethods.config.SelectSingleNode("Parameters/Application").InnerXml : "");
				if (str3 == "FINS")
				{
					this.finsRadioButton.Checked = true;
				}
				else if (str3 == "PORTAL")
				{
					this.loyaltyRadioButton.Checked = true;
				}
				bool.TryParse((commonMethods.config.SelectSingleNode("Parameters/DebugMode") != null ? commonMethods.config.SelectSingleNode("Parameters/DebugMode").InnerXml : "false"), out flag);
				if (!flag)
				{
					this.debugFlag.Checked = false;
				}
				else
				{
					this.debugFlag.Checked = true;
				}
				bool.TryParse((commonMethods.config.SelectSingleNode("Parameters/SQLTrace") != null ? commonMethods.config.SelectSingleNode("Parameters/SQLTrace").InnerXml : "false"), out flag1);
				if (!flag1)
				{
					this.sqlFlag.Checked = false;
				}
				else
				{
					this.sqlFlag.Checked = true;
				}
                this.SQLTracePath = (commonMethods.config.SelectSingleNode("Parameters/SQLTracePath") != null ? commonMethods.config.SelectSingleNode("Parameters/SQLTracePath").InnerXml : "");
				this.siebelPath = (commonMethods.config.SelectSingleNode("Parameters/SiebelInstallationPath") != null ? commonMethods.config.SelectSingleNode("Parameters/SiebelInstallationPath").InnerXml : "");
				this.DesaConnectionCfgName = (commonMethods.config.SelectSingleNode("Parameters/CFGConfig/DesaName") != null ? commonMethods.config.SelectSingleNode("Parameters/CFGConfig/DesaName").InnerXml : "");
				this.InteConnectionCfgName = (commonMethods.config.SelectSingleNode("Parameters/CFGConfig/InteName") != null ? commonMethods.config.SelectSingleNode("Parameters/CFGConfig/InteName").InnerXml : "");
                this.HomoConnectionCfgName = (commonMethods.config.SelectSingleNode("Parameters/CFGConfig/HomoName") != null ? commonMethods.config.SelectSingleNode("Parameters/CFGConfig/HomoName").InnerXml : "");
			}
			catch (FieldAccessException fieldAccessException1)
			{
				FieldAccessException fieldAccessException = fieldAccessException1;
				commonMethods.logMessage("Obteniendo parametros de archivo de configuración", null);
				commonMethods.logMessage(null, fieldAccessException);
			}
		}

		private List<string> fetchServersFromConfig()
		{
			commonMethods.config.Load(commonMethods.configFile);
			List<string> strs = new List<string>();
            foreach (XmlNode xmlNodes in commonMethods.config.SelectNodes("//Servers"))
			{
				foreach (XmlNode childNode in xmlNodes.ChildNodes)
				{
					strs.Add(childNode.Name);
				}
			}
			return strs;
		}

        private List<string> fetchTNSFromConfig()
        {
            commonMethods.config.Load(commonMethods.configFile);
            List<string> strs = new List<string>();
            foreach (XmlNode xmlNodes in commonMethods.config.SelectNodes("//TNSNames"))
            {
                foreach (XmlNode childNode in xmlNodes.ChildNodes)
                {
                    strs.Add(childNode.Name);
                }
            }
            return strs;
        }

        private List<string> fetchEnterprisesFromConfig()
        {
            commonMethods.config.Load(commonMethods.configFile);
            List<string> strs = new List<string>();
            foreach (XmlNode xmlNodes in commonMethods.config.SelectNodes("//Enterprise"))
            {
                foreach (XmlNode childNode in xmlNodes.ChildNodes)
                {
                    strs.Add(childNode.Name);
                }
            }
            return strs;
        }
		private string finalCommand()
        {
            string str = string.Concat(this.siebelPath, "\\BIN\\siebel.exe");
            string[] strArrays = new string[] { str, " ", this.envirormentSelected(), " ", this.browserSelected(), " ", this.applicationSelected(), " ", this.userSelected(), " ", this.debugSelected(), " ", this.traceSqlSelected()," " , this.WSOnlySelected() };
            return string.Concat(strArrays);

        }

		private void Form1_Load(object sender, EventArgs e)
		{
			this.progressBar1.Minimum = 0;
			this.progressBar1.Maximum = 100;
			this.progressBar1.Visible = false;
            this.copiarEndpoint.Visible = false;
            this.CenterToScreen();
			commonMethods.logMessage("--------------------------------", null);
			DateTime now = DateTime.Now;
			commonMethods.logMessage(string.Concat("Iniciando App, Fecha: ", now.ToString()), null);
			commonMethods.logMessage(string.Concat("Usuario: ", WindowsIdentity.GetCurrent().Name), null);
			commonMethods.logMessage(string.Concat("Leyendo parámetros desde tabla"), null);
            this.readConfigFile();      
			this.configWatcher();
            this.AppVersionLabel.Text = commonMethods.AppVersion;
            this.log_events.Invoke(new Action(() => this.log_events.Text = logLevel.ToString()));
			base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.createMenuAndItems();
			this.createIconMenuStructure();
            ToolTip toolTip1 = new ToolTip();
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 100;
            toolTip1.ReshowDelay = 500;
            toolTip1.ShowAlways = true;            

            if (File.Exists("C:\\Program Files\\Mozilla Firefox\\firefox.exe"))
            {
                this.firefoxRadioButton.Enabled = true;
                toolTip1.SetToolTip(this.firefoxRadioButton, "Unicamente si se encuentra Firefox");
            }
            else
            {                
                toolTip1.SetToolTip(this.firefoxRadioButton, "Firefox no se encontró instalado");
            }

			if (!Form1.IsAdministrator())
			{
				commonMethods.logMessage("El usuario logueado no es administrador de la pc.", null);
				DialogCenteringService dialogCenteringService = new DialogCenteringService(this);
				try
				{
					MessageBox.Show("Debe ser administrador de la pc para poder utilizar este aplicativo");
				}
				finally
				{
					if (dialogCenteringService != null)
					{
						((IDisposable)dialogCenteringService).Dispose();
					}
				}
				Environment.Exit(0);
			}
			commonMethods.logMessage("Iniciado", null);
            
            if (ConfigChanged)
            {
                MessageBox.Show("Debera abrir la configuracion para el correcto funcionamiento.");
            }

		}


		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.launchSiebel = new System.Windows.Forms.Button();
            this.desaRadioButton = new System.Windows.Forms.RadioButton();
            this.inteRadioButton = new System.Windows.Forms.RadioButton();
            this.ieRadioButton = new System.Windows.Forms.RadioButton();
            this.chromeRadioButton = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.userText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.debugFlag = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.homoRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.firefoxRadioButton = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.loyaltyRadioButton = new System.Windows.Forms.RadioButton();
            this.finsRadioButton = new System.Windows.Forms.RadioButton();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.label1 = new System.Windows.Forms.Label();
            this.sqlFlag = new System.Windows.Forms.CheckBox();
            this.configButton = new System.Windows.Forms.Button();
            this.bajarArchivosCustom = new System.Windows.Forms.Button();
            this.AppVersionLabel = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.matarSiebel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.wsFlag = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.copiarEndpoint = new System.Windows.Forms.Button();
            this.log_events = new System.Windows.Forms.TextBox();
            this.setLogEvents = new System.Windows.Forms.Button();
            this.matarIE = new System.Windows.Forms.Button();
            this.Tab = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.generarBrowserScr = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ServerInte02 = new System.Windows.Forms.RadioButton();
            this.ServerDesa03 = new System.Windows.Forms.RadioButton();
            this.ServerDesa02 = new System.Windows.Forms.RadioButton();
            this.syncSchema = new System.Windows.Forms.Button();
            this.exportDAT = new System.Windows.Forms.Button();
            this.fullcompileButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.Tab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // launchSiebel
            // 
            this.launchSiebel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.launchSiebel.Font = new System.Drawing.Font("MS Reference Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.launchSiebel.Location = new System.Drawing.Point(277, 162);
            this.launchSiebel.Name = "launchSiebel";
            this.launchSiebel.Size = new System.Drawing.Size(237, 42);
            this.launchSiebel.TabIndex = 0;
            this.launchSiebel.Text = "Lanzar Siebel Client";
            this.launchSiebel.UseVisualStyleBackColor = true;
            this.launchSiebel.Click += new System.EventHandler(this.button1_Click);
            // 
            // desaRadioButton
            // 
            this.desaRadioButton.AutoSize = true;
            this.desaRadioButton.Location = new System.Drawing.Point(13, 22);
            this.desaRadioButton.Name = "desaRadioButton";
            this.desaRadioButton.Size = new System.Drawing.Size(90, 20);
            this.desaRadioButton.TabIndex = 2;
            this.desaRadioButton.TabStop = true;
            this.desaRadioButton.Text = "Desarrollo";
            this.desaRadioButton.UseVisualStyleBackColor = true;
            // 
            // inteRadioButton
            // 
            this.inteRadioButton.AutoSize = true;
            this.inteRadioButton.Location = new System.Drawing.Point(13, 48);
            this.inteRadioButton.Name = "inteRadioButton";
            this.inteRadioButton.Size = new System.Drawing.Size(101, 20);
            this.inteRadioButton.TabIndex = 3;
            this.inteRadioButton.TabStop = true;
            this.inteRadioButton.Text = "Integración";
            this.inteRadioButton.UseVisualStyleBackColor = true;
            // 
            // ieRadioButton
            // 
            this.ieRadioButton.AutoSize = true;
            this.ieRadioButton.Location = new System.Drawing.Point(15, 25);
            this.ieRadioButton.Name = "ieRadioButton";
            this.ieRadioButton.Size = new System.Drawing.Size(39, 20);
            this.ieRadioButton.TabIndex = 5;
            this.ieRadioButton.TabStop = true;
            this.ieRadioButton.Text = "IE";
            this.ieRadioButton.UseVisualStyleBackColor = true;
            // 
            // chromeRadioButton
            // 
            this.chromeRadioButton.AutoSize = true;
            this.chromeRadioButton.Location = new System.Drawing.Point(15, 48);
            this.chromeRadioButton.Name = "chromeRadioButton";
            this.chromeRadioButton.Size = new System.Drawing.Size(75, 20);
            this.chromeRadioButton.TabIndex = 6;
            this.chromeRadioButton.TabStop = true;
            this.chromeRadioButton.Text = "Chrome";
            this.chromeRadioButton.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(559, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "Usuario:";
            // 
            // userText
            // 
            this.userText.BackColor = System.Drawing.Color.White;
            this.userText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.userText.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.userText.Location = new System.Drawing.Point(636, 35);
            this.userText.Name = "userText";
            this.userText.Size = new System.Drawing.Size(116, 23);
            this.userText.TabIndex = 8;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(527, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(94, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Modo debug:";
            // 
            // debugFlag
            // 
            this.debugFlag.AutoSize = true;
            this.debugFlag.Location = new System.Drawing.Point(636, 72);
            this.debugFlag.Name = "debugFlag";
            this.debugFlag.Size = new System.Drawing.Size(15, 14);
            this.debugFlag.TabIndex = 10;
            this.debugFlag.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.homoRadioButton);
            this.groupBox1.Controls.Add(this.desaRadioButton);
            this.groupBox1.Controls.Add(this.inteRadioButton);
            this.groupBox1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(28, 38);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(138, 101);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ambiente:";
            // 
            // homoRadioButton
            // 
            this.homoRadioButton.AutoSize = true;
            this.homoRadioButton.Location = new System.Drawing.Point(13, 74);
            this.homoRadioButton.Name = "homoRadioButton";
            this.homoRadioButton.Size = new System.Drawing.Size(116, 20);
            this.homoRadioButton.TabIndex = 27;
            this.homoRadioButton.TabStop = true;
            this.homoRadioButton.Text = "Homologación";
            this.homoRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.firefoxRadioButton);
            this.groupBox2.Controls.Add(this.chromeRadioButton);
            this.groupBox2.Controls.Add(this.ieRadioButton);
            this.groupBox2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(188, 38);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(128, 101);
            this.groupBox2.TabIndex = 12;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Navegador:";
            // 
            // firefoxRadioButton
            // 
            this.firefoxRadioButton.AutoSize = true;
            this.firefoxRadioButton.Enabled = false;
            this.firefoxRadioButton.Location = new System.Drawing.Point(15, 71);
            this.firefoxRadioButton.Name = "firefoxRadioButton";
            this.firefoxRadioButton.Size = new System.Drawing.Size(70, 20);
            this.firefoxRadioButton.TabIndex = 7;
            this.firefoxRadioButton.TabStop = true;
            this.firefoxRadioButton.Text = "Firefox";
            this.firefoxRadioButton.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.loyaltyRadioButton);
            this.groupBox3.Controls.Add(this.finsRadioButton);
            this.groupBox3.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(338, 38);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(146, 101);
            this.groupBox3.TabIndex = 13;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Aplicación";
            // 
            // loyaltyRadioButton
            // 
            this.loyaltyRadioButton.AutoSize = true;
            this.loyaltyRadioButton.Location = new System.Drawing.Point(13, 53);
            this.loyaltyRadioButton.Name = "loyaltyRadioButton";
            this.loyaltyRadioButton.Size = new System.Drawing.Size(117, 20);
            this.loyaltyRadioButton.TabIndex = 1;
            this.loyaltyRadioButton.TabStop = true;
            this.loyaltyRadioButton.Text = "Loyalty Portal";
            this.loyaltyRadioButton.UseVisualStyleBackColor = true;
            // 
            // finsRadioButton
            // 
            this.finsRadioButton.AutoSize = true;
            this.finsRadioButton.Location = new System.Drawing.Point(13, 25);
            this.finsRadioButton.Name = "finsRadioButton";
            this.finsRadioButton.Size = new System.Drawing.Size(57, 20);
            this.finsRadioButton.TabIndex = 0;
            this.finsRadioButton.TabStop = true;
            this.finsRadioButton.Text = "FINS";
            this.finsRadioButton.UseVisualStyleBackColor = true;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenuStrip = this.contextMenu;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Siebel Client Launcher";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // contextMenu
            // 
            this.contextMenu.BackColor = System.Drawing.Color.WhiteSmoke;
            this.contextMenu.Name = "contextMenu";
            this.contextMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(538, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "SQL Trace:";
            // 
            // sqlFlag
            // 
            this.sqlFlag.AutoSize = true;
            this.sqlFlag.Location = new System.Drawing.Point(636, 100);
            this.sqlFlag.Name = "sqlFlag";
            this.sqlFlag.Size = new System.Drawing.Size(15, 14);
            this.sqlFlag.TabIndex = 16;
            this.sqlFlag.UseVisualStyleBackColor = true;
            // 
            // configButton
            // 
            this.configButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.configButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configButton.Location = new System.Drawing.Point(201, 8);
            this.configButton.Name = "configButton";
            this.configButton.Size = new System.Drawing.Size(115, 28);
            this.configButton.TabIndex = 18;
            this.configButton.Text = "Configuración";
            this.configButton.UseVisualStyleBackColor = true;
            this.configButton.Click += new System.EventHandler(this.configLocationButton_Click);
            // 
            // bajarArchivosCustom
            // 
            this.bajarArchivosCustom.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bajarArchivosCustom.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bajarArchivosCustom.Location = new System.Drawing.Point(16, 224);
            this.bajarArchivosCustom.Name = "bajarArchivosCustom";
            this.bajarArchivosCustom.Size = new System.Drawing.Size(164, 28);
            this.bajarArchivosCustom.TabIndex = 19;
            this.bajarArchivosCustom.Text = "Bajar archivos Custom";
            this.bajarArchivosCustom.UseVisualStyleBackColor = true;
            this.bajarArchivosCustom.Click += new System.EventHandler(this.button2_Click);
            // 
            // AppVersionLabel
            // 
            this.AppVersionLabel.AutoSize = true;
            this.AppVersionLabel.Location = new System.Drawing.Point(759, 237);
            this.AppVersionLabel.Name = "AppVersionLabel";
            this.AppVersionLabel.Size = new System.Drawing.Size(19, 15);
            this.AppVersionLabel.TabIndex = 20;
            this.AppVersionLabel.Text = "...";
            // 
            // progressBar1
            // 
            this.progressBar1.ForeColor = System.Drawing.SystemColors.GrayText;
            this.progressBar1.Location = new System.Drawing.Point(322, 8);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(194, 28);
            this.progressBar1.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar1.TabIndex = 21;
            // 
            // matarSiebel
            // 
            this.matarSiebel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.matarSiebel.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matarSiebel.Location = new System.Drawing.Point(367, 224);
            this.matarSiebel.Name = "matarSiebel";
            this.matarSiebel.Size = new System.Drawing.Size(143, 28);
            this.matarSiebel.TabIndex = 22;
            this.matarSiebel.Text = "Matar Siebel.exe";
            this.matarSiebel.UseVisualStyleBackColor = true;
            this.matarSiebel.Click += new System.EventHandler(this.button3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 14);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(94, 16);
            this.label2.TabIndex = 24;
            this.label2.Text = "Nivel de Log:";
            // 
            // wsFlag
            // 
            this.wsFlag.AutoSize = true;
            this.wsFlag.Location = new System.Drawing.Point(636, 129);
            this.wsFlag.Name = "wsFlag";
            this.wsFlag.Size = new System.Drawing.Size(15, 14);
            this.wsFlag.TabIndex = 25;
            this.wsFlag.UseVisualStyleBackColor = true;
            this.wsFlag.CheckedChanged += new System.EventHandler(this.wsFlag_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(496, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 16);
            this.label5.TabIndex = 26;
            this.label5.Text = "WebService Solo:";
            // 
            // copiarEndpoint
            // 
            this.copiarEndpoint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.copiarEndpoint.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.copiarEndpoint.Location = new System.Drawing.Point(571, 173);
            this.copiarEndpoint.Name = "copiarEndpoint";
            this.copiarEndpoint.Size = new System.Drawing.Size(154, 26);
            this.copiarEndpoint.TabIndex = 27;
            this.copiarEndpoint.Text = "Copiar Endpoint Local";
            this.copiarEndpoint.UseVisualStyleBackColor = true;
            this.copiarEndpoint.Click += new System.EventHandler(this.button4_Click);
            // 
            // log_events
            // 
            this.log_events.BackColor = System.Drawing.Color.White;
            this.log_events.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.log_events.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.log_events.Location = new System.Drawing.Point(113, 10);
            this.log_events.Name = "log_events";
            this.log_events.Size = new System.Drawing.Size(29, 23);
            this.log_events.TabIndex = 28;
            this.log_events.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // setLogEvents
            // 
            this.setLogEvents.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.setLogEvents.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.setLogEvents.Location = new System.Drawing.Point(148, 8);
            this.setLogEvents.Name = "setLogEvents";
            this.setLogEvents.Size = new System.Drawing.Size(41, 28);
            this.setLogEvents.TabIndex = 29;
            this.setLogEvents.Text = "Set";
            this.setLogEvents.UseVisualStyleBackColor = true;
            this.setLogEvents.Click += new System.EventHandler(this.setLogEvents_Click);
            // 
            // matarIE
            // 
            this.matarIE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.matarIE.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.matarIE.Location = new System.Drawing.Point(516, 224);
            this.matarIE.Name = "matarIE";
            this.matarIE.Size = new System.Drawing.Size(143, 28);
            this.matarIE.TabIndex = 30;
            this.matarIE.Text = "Matar iexplore.exe";
            this.matarIE.UseVisualStyleBackColor = true;
            this.matarIE.Click += new System.EventHandler(this.button5_Click);
            // 
            // Tab
            // 
            this.Tab.Controls.Add(this.tabPage1);
            this.Tab.Controls.Add(this.tabPage2);
            this.Tab.Location = new System.Drawing.Point(0, 21);
            this.Tab.Name = "Tab";
            this.Tab.SelectedIndex = 0;
            this.Tab.Size = new System.Drawing.Size(796, 296);
            this.Tab.TabIndex = 31;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.generarBrowserScr);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.launchSiebel);
            this.tabPage1.Controls.Add(this.matarIE);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.setLogEvents);
            this.tabPage1.Controls.Add(this.userText);
            this.tabPage1.Controls.Add(this.log_events);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.copiarEndpoint);
            this.tabPage1.Controls.Add(this.debugFlag);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Controls.Add(this.wsFlag);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox3);
            this.tabPage1.Controls.Add(this.matarSiebel);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.progressBar1);
            this.tabPage1.Controls.Add(this.sqlFlag);
            this.tabPage1.Controls.Add(this.AppVersionLabel);
            this.tabPage1.Controls.Add(this.configButton);
            this.tabPage1.Controls.Add(this.bajarArchivosCustom);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(788, 268);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Launcher";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // generarBrowserScr
            // 
            this.generarBrowserScr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.generarBrowserScr.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.generarBrowserScr.Location = new System.Drawing.Point(186, 224);
            this.generarBrowserScr.Name = "generarBrowserScr";
            this.generarBrowserScr.Size = new System.Drawing.Size(175, 28);
            this.generarBrowserScr.TabIndex = 31;
            this.generarBrowserScr.Text = "Generar BrowserScript";
            this.generarBrowserScr.UseVisualStyleBackColor = true;
            this.generarBrowserScr.Click += new System.EventHandler(this.generarBS_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Controls.Add(this.groupBox4);
            this.tabPage2.Controls.Add(this.syncSchema);
            this.tabPage2.Controls.Add(this.exportDAT);
            this.tabPage2.Controls.Add(this.fullcompileButton);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(788, 268);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Repositorios";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Red;
            this.label6.Location = new System.Drawing.Point(413, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(248, 19);
            this.label6.TabIndex = 26;
            this.label6.Text = "Version ALPHA usar con cuidado";
            // 
            // textBox1
            // 
            this.textBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox1.Location = new System.Drawing.Point(12, 111);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox1.Size = new System.Drawing.Size(765, 150);
            this.textBox1.TabIndex = 0;
            this.textBox1.WordWrap = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ServerInte02);
            this.groupBox4.Controls.Add(this.ServerDesa03);
            this.groupBox4.Controls.Add(this.ServerDesa02);
            this.groupBox4.Location = new System.Drawing.Point(25, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(256, 99);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Servidor";
            // 
            // ServerInte02
            // 
            this.ServerInte02.AutoSize = true;
            this.ServerInte02.Location = new System.Drawing.Point(21, 70);
            this.ServerInte02.Name = "ServerInte02";
            this.ServerInte02.Size = new System.Drawing.Size(167, 19);
            this.ServerInte02.TabIndex = 2;
            this.ServerInte02.TabStop = true;
            this.ServerInte02.Text = "Integración (IAPPSBL02)";
            this.ServerInte02.UseVisualStyleBackColor = true;
            // 
            // ServerDesa03
            // 
            this.ServerDesa03.AutoSize = true;
            this.ServerDesa03.Location = new System.Drawing.Point(21, 45);
            this.ServerDesa03.Name = "ServerDesa03";
            this.ServerDesa03.Size = new System.Drawing.Size(161, 19);
            this.ServerDesa03.TabIndex = 1;
            this.ServerDesa03.TabStop = true;
            this.ServerDesa03.Text = "BugFixing (DAPPSBL03)";
            this.ServerDesa03.UseVisualStyleBackColor = true;
            // 
            // ServerDesa02
            // 
            this.ServerDesa02.AutoSize = true;
            this.ServerDesa02.Location = new System.Drawing.Point(21, 20);
            this.ServerDesa02.Name = "ServerDesa02";
            this.ServerDesa02.Size = new System.Drawing.Size(165, 19);
            this.ServerDesa02.TabIndex = 0;
            this.ServerDesa02.TabStop = true;
            this.ServerDesa02.Text = "Desarrollo (DAPPSBL02)";
            this.ServerDesa02.UseVisualStyleBackColor = true;
            // 
            // syncSchema
            // 
            this.syncSchema.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.syncSchema.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.syncSchema.Location = new System.Drawing.Point(615, 40);
            this.syncSchema.Name = "syncSchema";
            this.syncSchema.Size = new System.Drawing.Size(143, 28);
            this.syncSchema.TabIndex = 25;
            this.syncSchema.Text = "Sync Schema";
            this.syncSchema.UseVisualStyleBackColor = true;
            this.syncSchema.Click += new System.EventHandler(this.syncSchema_Click);
            // 
            // exportDAT
            // 
            this.exportDAT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.exportDAT.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.exportDAT.Location = new System.Drawing.Point(466, 40);
            this.exportDAT.Name = "exportDAT";
            this.exportDAT.Size = new System.Drawing.Size(143, 28);
            this.exportDAT.TabIndex = 24;
            this.exportDAT.Text = "Exportar DAT";
            this.exportDAT.UseVisualStyleBackColor = true;
            this.exportDAT.Click += new System.EventHandler(this.exportDat_Click);
            // 
            // fullcompileButton
            // 
            this.fullcompileButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.fullcompileButton.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fullcompileButton.Location = new System.Drawing.Point(317, 40);
            this.fullcompileButton.Name = "fullcompileButton";
            this.fullcompileButton.Size = new System.Drawing.Size(143, 28);
            this.fullcompileButton.TabIndex = 23;
            this.fullcompileButton.Text = "Full Compile";
            this.fullcompileButton.UseVisualStyleBackColor = true;
            this.fullcompileButton.Click += new System.EventHandler(this.fullCompile_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(794, 315);
            this.Controls.Add(this.Tab);
            this.Font = new System.Drawing.Font("MS Reference Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Siebel Client Launcher";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.Tab.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

		}

		public static bool IsAdministrator()
		{
			return (new WindowsPrincipal(WindowsIdentity.GetCurrent())).IsInRole(WindowsBuiltInRole.Administrator);
		}



        private void showAboutApp(object sender, EventArgs e)
        {
            DialogCenteringService dialogCenteringService = new DialogCenteringService(this);
            try
            {
                string[] newLine = new string[] { "1- Abrir el .config y parametrizar", Environment.NewLine, "2-'Nivelar customizaciones': Bajada local de", 
                        Environment.NewLine, "*Javascript", Environment.NewLine, "*CSS", Environment.NewLine, "*Imagenes", 
                        Environment.NewLine, 
                        Environment.NewLine, "Codigo Fuente: https://bitbucket.org/lasmaty07/siebellauncher/overview" ,
                        Environment.NewLine, 
                        Environment.NewLine, "Lucas Pehuén Solari, Matias Brigante" ,
                        Environment.NewLine, 
                        Environment.NewLine, "¿Desea ir a la pagina con el codigo fuente?" };
                if (MessageBox.Show(string.Concat(newLine), "Acerca de", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.Yes )
                {
                    System.Diagnostics.Process.Start("https://bitbucket.org/lasmaty07/siebellauncher/overview");
                }
            }
            finally
            {
                if (dialogCenteringService != null)
                {
                    ((IDisposable)dialogCenteringService).Dispose();
                }
            }
        }

		private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
		{
			base.Show();
			base.WindowState = FormWindowState.Normal;
			base.Activate();
		}

		private void OnConfigChanged(object sender, FileSystemEventArgs e)
		{
			Form1 form1 = this;
			form1.fireCount = form1.fireCount + 1;
			if (this.fireCount != 1)
			{
				this.fireCount = 0;
			}
			else
			{
				commonMethods.logMessage("Archivo de configuración modificado", null);                
				System.Windows.Forms.Cursor.Current = Cursors.WaitCursor;
				Thread.Sleep(0x3e8);
				this.fetchConfigFromFile();
				base.Invoke(new Action(() => this.createMenuAndItems()));
				System.Windows.Forms.Cursor.Current = Cursors.Default;
                if (!this.validatePath())
                {
                    commonMethods.logMessage("Path seleccionado de instalacion invalido. ", null); 
                    MessageBox.Show(string.Concat("Ruta dentro de <SiebelInstallationPath> Incorrecta, no se encontro \\BIN\\Siebel.exe ", Environment.NewLine,"Recuerde Configurar hasta la carpeta CLIENT"));
                }
			}
		}

		protected override void OnFormClosing(FormClosingEventArgs e)
		{
			base.OnFormClosing(e);
			e.Cancel = true;
			base.Hide();
            this.notifyIcon1.BalloonTipText = "Sigo abierto por acá...";
            this.notifyIcon1.ShowBalloonTip(15);
		}

		private void readConfigFile()
		{

			if (!File.Exists(commonMethods.configFile))
			{
                commonMethods.createXMLFile(null, null, null, null, null);
                try
                {
                    commonMethods.config.Load(commonMethods.configFile);
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage("Error al leer .config: ", exception.InnerException);
                }
				this.fetchConfigFromFile();
			}
			else
			{
                try
                {
                    commonMethods.config.Load(commonMethods.configFile);
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage("Error al leer .config: ", exception.InnerException);                 
                }
                checkConfigVersion();
				this.fetchConfigFromFile();
			}
		}
        
        // ESC key closes de form (still shows on system tray)
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (Form1.ModifierKeys == Keys.None && keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }
        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }
		private void SafeInvoke(Form1.SafeInvokeDelegate method)
		{
			if (!base.InvokeRequired)
			{
				method();
			}
			else
			{
				base.Invoke(method);
			}
		}

		private string traceSqlSelected()
		{
			return (this.sqlFlag.Checked ? string.Concat("/s ",this.SQLTracePath ,"\\siebel_sql.txt") : "");
		}

        private string WSOnlySelected()
        {
            return (this.wsFlag.Checked ? "/webservice 2330 " : "");
        }

		private string userSelected()
		{
			string str = string.Concat("/u ", this.userText.Text, " /p ", this.userText.Text);
			return str;
		}

		private bool validateRunningProcesses()
		{
			DialogCenteringService dialogCenteringService;
			Process process;
			Process[] processArray;
			int i;
            if (this.ieRadioButton.Checked)
            {
                commonMethods.logMessage("Comprobando si existen sesiones de iexplore abiertas ", null); 
                Process[] processesByName = Process.GetProcessesByName("iexplore");
                if ((int)processesByName.Length > 0)
                {
                    dialogCenteringService = new DialogCenteringService(this);
                    try
                    {
                        if (MessageBox.Show("Existen sesiones de IE abiertas. ¿Desea cerrarlas?", "Sesiones IE", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            processArray = processesByName;
                            for (i = 0; i < (int)processArray.Length; i++)
                            {
                                process = processArray[i];
                                if (!process.HasExited)
                                {
                                    process.Kill();
                                    commonMethods.logMessage(string.Concat("Matando IE process ID ", process.Id), null);
                                }
                            }
                            return true;
                        }
                        else
                        {
                            commonMethods.logMessage("Se canceló el kill de las sesiones de iexplore ", null); 
                            return false;
                        }
                    }
                    finally
                    {
                        if (dialogCenteringService != null)
                        {
                            ((IDisposable)dialogCenteringService).Dispose();
                        }
                    }
                } //no hay processos corriendo
                else
                {
                    return true;
                }
            }
            else
            {
                if (this.chromeRadioButton.Checked)
                {
                    commonMethods.logMessage("Comprobando si existen sesiones de chrome abiertas ", null); 
                    Process[] processesByName1 = Process.GetProcessesByName("chrome");
                    if ((int)processesByName1.Length > 0)
                    {
                        dialogCenteringService = new DialogCenteringService(this);
                        try
                        {
                            if (MessageBox.Show("Existen sesiones de Chrome abiertas. ¿Desea cerrarlas?", "Sesiones Chrome", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                            {
                                processArray = processesByName1;
                                for (i = 0; i < (int)processArray.Length; i++)
                                {
                                    process = processArray[i];
                                    if (!process.HasExited)
                                    {
                                        process.Kill();
                                        commonMethods.logMessage(string.Concat("Matando Chrome process ID ", process.Id), null);
                                    }
                                }
                                return true;
                            }
                            else
                            {
                                commonMethods.logMessage("Se canceló el kill de las sesiones de chrome ", null); 
                                return false;
                            }
                        }
                        finally
                        {
                            if (dialogCenteringService != null)
                            {
                                ((IDisposable)dialogCenteringService).Dispose();
                            }
                        }
                    } //no hay processos corriendo
                    else
                    {
                        return true;
                    }
                }
                else
                {
                    if (this.firefoxRadioButton.Checked)
                    {
                        commonMethods.logMessage("Comprobando si existen sesiones de firefox abiertas ", null); 
                        Process[] processesByName1 = Process.GetProcessesByName("firefox");
                        if ((int)processesByName1.Length > 0)
                        {
                            dialogCenteringService = new DialogCenteringService(this);
                            try
                            {
                                if (MessageBox.Show("Existen sesiones de Firefox abiertas. ¿Desea cerrarlas?", "Sesiones Firefox", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                                {
                                    processArray = processesByName1;
                                    for (i = 0; i < (int)processArray.Length; i++)
                                    {
                                        process = processArray[i];
                                        if (!process.HasExited)
                                        {
                                            process.Kill();
                                            commonMethods.logMessage(string.Concat("Matando Firefox process ID ", process.Id), null);
                                        }
                                    }
                                    return true;
                                }
                                else
                                {
                                    commonMethods.logMessage("Se canceló el kill de las sesiones de Firefox ", null); 
                                    return false;
                                }
                            }
                            finally
                            {
                                if (dialogCenteringService != null)
                                {
                                    ((IDisposable)dialogCenteringService).Dispose();
                                }
                            }
                        } //no hay processos corriendo
                        else
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
            }
		}

		private bool validateSelection()
		{
			bool flag = false;
            if (((this.desaRadioButton.Checked || this.inteRadioButton.Checked || this.homoRadioButton.Checked) && (this.ieRadioButton.Checked || this.chromeRadioButton.Checked || this.firefoxRadioButton.Checked) ? !string.IsNullOrEmpty(this.userText.Text) : false))
			{
				flag = true;
			}
			return flag;
		}

        private bool validatePath()
        { 
            bool flag = false;
            if (File.Exists(string.Concat(this.siebelPath, "\\BIN\\siebel.exe")))
            {
                commonMethods.logMessage("Path de Instalacion válido ", null); 
                flag = true;
            }
            return flag;
        }

		private void wc_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
		{
			DialogCenteringService dialogCenteringService = new DialogCenteringService(this);
			try
			{
				MessageBox.Show("Archivo copiado correctamente");
			}
			finally
			{
				if (dialogCenteringService != null)
				{
					((IDisposable)dialogCenteringService).Dispose();
				}
			}
			this.SafeInvoke(() => this.progressBar1.Visible = false);
			this.copiadoEnCurso = false;
		}

		private void wc_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			this.copiadoEnCurso = true;
			this.SafeInvoke(() => this.progressBar1.Value = e.ProgressPercentage);
		}

		private delegate void SafeInvokeDelegate();

        

        private void openLogFile(object sender, EventArgs e)
        {
            try
            {
                Process.Start(commonMethods.logErrorFile);
            }
            catch (Exception exception)
            {
                commonMethods.logMessage("Error al abrir la carpeta de la aplicacion", exception.InnerException);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            DialogCenteringService dialogCenteringService;
            Process process;
            Process[] processArray;
            int i;            
            Process[] processesByName = Process.GetProcessesByName("siebel");
            if ((int)processesByName.Length > 0)
            {
                dialogCenteringService = new DialogCenteringService(this);
                try
                {                    
                    if (MessageBox.Show("¿Desea cerrar las sesiones de Siebel Mobile Client?", "Sesiones Siebel Mobile Client", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        processArray = processesByName;
                        for (i = 0; i < (int)processArray.Length; i++)
                        {
                            process = processArray[i];
                            if (!process.HasExited)
                            {
                                process.Kill();
                                commonMethods.logMessage(string.Concat("Matando Siebel.exe process ID ", process.Id), null);
                            }
                        }
                    }
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
            else
            {
                dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    MessageBox.Show("No Existen Sesiones de Siebel Mobile Client en ejecución", "Siebel Mobile Client", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
        }
        private static DialogResult ShowInputDialog(ref string input)
        {
            System.Drawing.Size size = new System.Drawing.Size(200, 70);
            Form1 inputBox = new Form1();

            inputBox.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            inputBox.ClientSize = size;
            inputBox.Text = "Contraseña Produccion";

            System.Windows.Forms.TextBox textBox = new TextBox();
            textBox.Size = new System.Drawing.Size(size.Width - 10, 23);
            textBox.Location = new System.Drawing.Point(5, 5);
            textBox.Text = input;
            inputBox.Controls.Add(textBox);

            Button okButton = new Button();
            okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            okButton.Name = "okButton";
            okButton.Size = new System.Drawing.Size(75, 23);
            okButton.Text = "&OK";
            okButton.Location = new System.Drawing.Point(size.Width - 80 - 80, 39);
            inputBox.Controls.Add(okButton);

            Button cancelButton = new Button();
            cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new System.Drawing.Size(75, 23);
            cancelButton.Text = "&Cancel";
            cancelButton.Location = new System.Drawing.Point(size.Width - 80, 39);
            inputBox.Controls.Add(cancelButton);

            inputBox.AcceptButton = okButton;
            inputBox.CancelButton = cancelButton;
            inputBox.StartPosition = FormStartPosition.CenterParent;

            DialogResult result = inputBox.ShowDialog();
            input = textBox.Text;
            return result;
        }

        private void wsFlag_CheckedChanged(object sender, EventArgs e)
        {
            if (this.wsFlag.Checked)
            {
                MessageBox.Show(string.Concat("Recuerde que debe tambien configurar el CFG con ", Environment.NewLine, "EnableWebServices = TRUE", Environment.NewLine, "WebServicesPort = 2330"));
                this.copiarEndpoint.Visible = true;
            }

            if (!this.wsFlag.Checked)
                this.copiarEndpoint.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            String hostName = Dns.GetHostName();
            string endpoint = string.Concat("http://", hostName, ".bancogalicia.com.ar:2330/eai/start.swe?SWEExtSource=SecureWebService&SWEExtCmd=Execute");
            commonMethods.logMessage(string.Concat("Copiado a clipboard el endpoint local: ", endpoint), null);
            Clipboard.SetText(endpoint);
        }

        private void setLogEvents_Click(object sender, EventArgs e)
        {
            string logEv = this.log_events.Text;
            if (logEv == "0" || logEv == "1" || logEv == "2" || logEv == "3" || logEv == "4" || logEv == "5")
            {
                try
                {
                    commonMethods.logMessage(string.Concat("Seteando variable de entorno SIEBEL_LOG_EVENTS a: ", logEv), null);
                    Environment.SetEnvironmentVariable("SIEBEL_LOG_EVENTS", logEv, EnvironmentVariableTarget.Machine);
                    Environment.SetEnvironmentVariable("SIEBEL_LOG_EVENTS", logEv, EnvironmentVariableTarget.User);
                    DialogCenteringService dialogCenteringService;
                    dialogCenteringService = new DialogCenteringService(this);
                    try
                    {
                        MessageBox.Show("Seteado Ok");
                    }
                    finally
                    {
                        if (dialogCenteringService != null)
                        {
                            ((IDisposable)dialogCenteringService).Dispose();
                        }
                    }
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage("Error al setear variable de sistema.", exception.InnerException);
                }
            }
            else 
            {
                DialogCenteringService dialogCenteringService;
                dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    MessageBox.Show("Nivel de Log erroneo, debe comprender un valor entre 0 y 5 ");
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DialogCenteringService dialogCenteringService;
            Process process;
            Process[] processArray;
            int i;
            Process[] processesByName = Process.GetProcessesByName("iexplore");
            if ((int)processesByName.Length > 0)
            {
                dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    if (MessageBox.Show("¿Desea cerrar las sesiones de Internet Explorer?", "IExplore", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        processArray = processesByName;
                        for (i = 0; i < (int)processArray.Length; i++)
                        {
                            process = processArray[i];
                            if (!process.HasExited)
                            {
                                process.Kill();
                                commonMethods.logMessage(string.Concat("Matando iexplore.exe process ID ", process.Id), null);
                            }
                        }
                    }
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
            else
            {
                dialogCenteringService = new DialogCenteringService(this);
                try
                {
                    MessageBox.Show("No Existen Sesiones de Internet Explorer en ejecución", "IExplore", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
        }

        private void exportDat_Click(object sender, EventArgs e)
        {

            DialogCenteringService dialogCenteringService;
            dialogCenteringService = new DialogCenteringService(this);

            if (!this.ServerDesa02.Checked && !this.ServerDesa03.Checked && !this.ServerInte02.Checked)
            {
                try
                {
                    MessageBox.Show("Debe Seleccionar un servidor.", "Server", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                finally
                {
                    if (dialogCenteringService != null)
                    {
                        ((IDisposable)dialogCenteringService).Dispose();
                    }
                }
            }
            else
            {
                if (this.ServerDesa02.Checked)
                {
                    commonMethods.ServerConfig("Desa02");                    
                }
                if (this.ServerDesa03.Checked)
                {
                    commonMethods.ServerConfig("Desa03");
                }
                if (this.ServerInte02.Checked)
                {
                    commonMethods.ServerConfig("Inte02");                    
                }
                //string file = "\"\\\\Dappsbl01\\App Pasajes Repo\\libs\\psExec\"";
                string file = "C:\\WINDOWS\\system32\\cmd.exe";
                //string arguments = "\\\\dappsbl03 CMD /c \"" + commonMethods.serverPath + " /a E /u SADMIN /p " + commonMethods.passwd + " /c " + commonMethods.alias + " /d SIEBEL /r ^\"Siebel Repository\" /f " + commonMethods.fileExport + " /W ESN /l " + commonMethods.fileLog + "\"";
                string arguments = "/c \"dir\""; 

                

                Process startInfo = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = file,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true 
                    }
                };
                startInfo.EnableRaisingEvents = true;
                startInfo.OutputDataReceived += ProcessDataHandler;
                startInfo.ErrorDataReceived += ProcessDataHandler;
                startInfo.Exited += new EventHandler(this.process_Exited);                
                
                if (MessageBox.Show("Seguro que desea Exportar el DAT en el servidor seleccionado. Esto puede tardar unos minutos.", "Exportar DAT", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    commonMethods.logMessage(string.Concat("Exportar DAT: ", file, " ", arguments), null);
                    try
                    {
                        startInfo.Start();
                        this.exportDAT.Enabled = false;
                        this.fullcompileButton.Enabled = false;
                        this.syncSchema.Enabled = false;
                        while (!startInfo.HasExited)
                        {
                            string s = File.ReadAllText("\\\\DAPPSBL03\\t$\\Siebel\\Compilacion\\DAT\\DAPPSBL03.log");
                            this.textBox1.Text = s;
                            Application.DoEvents(); // This keeps your form responsive by processing events
                        }
                        this.exportDAT.Enabled = true;
                        this.fullcompileButton.Enabled = true;
                        this.syncSchema.Enabled = true;
                    }
                    catch (Exception exception)
                    {
                        commonMethods.logMessage(string.Concat("Error al exportar DAT: "), exception.InnerException);
                    }
                    
                }
            }
        }
        

        private void fullCompile_Click(object sender, EventArgs e)
        {            
            DialogCenteringService dialogCenteringService;
            dialogCenteringService = new DialogCenteringService(this);
            try
            {
                MessageBox.Show("Falta Aun.... awantia manija", "Dev Pending", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                if (dialogCenteringService != null)
                {
                    ((IDisposable)dialogCenteringService).Dispose();
                }
            }
        }

        private void syncSchema_Click(object sender, EventArgs e)
        {

            DialogCenteringService dialogCenteringService;
            dialogCenteringService = new DialogCenteringService(this);
            try
            {
                MessageBox.Show("Falta Aun.... awantia manija", "Dev Pending", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            finally
            {
                if (dialogCenteringService != null)
                {
                    ((IDisposable)dialogCenteringService).Dispose();
                }
            }

            /*
            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "cmd.exe",
                    Arguments = "/c \"ping www.google.com\"",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            proc.EnableRaisingEvents = true;
            proc.Exited += new EventHandler(this.process_Exited);
            proc.Start();*/


        }


        private void process_Exited(object sender, System.EventArgs e)
        {
            commonMethods.logMessage(string.Concat("Proceso finalizado "));
            MessageBox.Show("El proceso en ejecucion ha fanalizado, por favor revisar los logs", "Finalizado", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            
        }


        private static void ProcessDataHandler(object sendingProcess, DataReceivedEventArgs outLine)
        {
            // Collect the net view command output. 
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                // Add the text to the collected output.
                m_sbText.AppendLine(outLine.Data);
            }
        }

        private void generarBS_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("¿Seguro que desea generar los browser script de Fins.cfg? Recuerde que no debe existir el directorio", "BrowserScript", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
            {
                Process process = new Process();
                ProcessStartInfo processStartInfo = new ProcessStartInfo()
                {
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = string.Concat(this.siebelPath, "\\BIN\\genbscript.exe"),
                    Arguments = string.Concat(this.siebelPath, this.finscfgpath, " ", this.siebelPath , "\\PUBLIC\\esn ", " ESN")

                };                
                process.EnableRaisingEvents = true;
                process.Exited += new EventHandler(this.process_Exited);
                process.StartInfo = processStartInfo;
                commonMethods.logMessage(string.Concat("generar browser Script en: ", processStartInfo.FileName, " ", processStartInfo.Arguments));
                try
                {                    
                    process.Start();                    
                }
                catch (Exception exception)
                {
                    commonMethods.logMessage(string.Concat("Error al generar BrowserScripts: "), exception.InnerException);
                }
                while (!process.HasExited)
                {
                    Application.DoEvents();
                }
                commonMethods.logMessage(string.Concat(Environment.NewLine,process.StandardOutput.ReadToEnd()));
            }
        }
	}
}